-- -----------------------------------------------------
-- Schema siscoord
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `siscoord` ;

-- -----------------------------------------------------
-- Schema siscoord
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `siscoord` DEFAULT CHARACTER SET utf8 ;
USE `siscoord` ;

-- -----------------------------------------------------
-- Table `siscoord`.`Pessoa`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `siscoord`.`Pessoa` ;

CREATE TABLE IF NOT EXISTS `siscoord`.`Pessoa` (
  `idPessoa` INT NOT NULL AUTO_INCREMENT,
  `nmPessoa` VARCHAR(110) NOT NULL,
  `cpf` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`idPessoa`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `siscoord`.`Discente`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `siscoord`.`Discente` ;

CREATE TABLE IF NOT EXISTS `siscoord`.`Discente` (
  `idDiscente` INT NOT NULL AUTO_INCREMENT,
  `idPessoa` INT NOT NULL,
  PRIMARY KEY (`idDiscente`),
  INDEX `fk_Discente_Pessoa_idx` (`idPessoa` ASC),
  CONSTRAINT `fk_Discente_Pessoa`
    FOREIGN KEY (`idPessoa`)
    REFERENCES `siscoord`.`Pessoa` (`idPessoa`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `siscoord`.`Professor`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `siscoord`.`Professor` ;

CREATE TABLE IF NOT EXISTS `siscoord`.`Professor` (
  `idProfessor` INT NOT NULL AUTO_INCREMENT,
  `idPessoa` INT NOT NULL,
  `matricula` VARCHAR(12) NOT NULL,
  `area` VARCHAR(45) NOT NULL,
  `user` VARCHAR(400) NOT NULL,
  `passWd` VARCHAR(400) NOT NULL,
  PRIMARY KEY (`idProfessor`, `user`),
  INDEX `fk_Professor_Pessoa1_idx` (`idPessoa` ASC),
  UNIQUE INDEX `user_UNIQUE` (`user` ASC),
  CONSTRAINT `fk_Professor_Pessoa1`
    FOREIGN KEY (`idPessoa`)
    REFERENCES `siscoord`.`Pessoa` (`idPessoa`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `siscoord`.`Portaria`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `siscoord`.`Portaria` ;

CREATE TABLE IF NOT EXISTS `siscoord`.`Portaria` (
  `idPortaria` INT NOT NULL AUTO_INCREMENT,
  `nroPortaria` VARCHAR(45) NOT NULL,
  `src` VARCHAR(400) NULL,
  `tipo` VARCHAR(45) NULL,
  PRIMARY KEY (`idPortaria`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `siscoord`.`Professor_has_Portaria`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `siscoord`.`Professor_has_Portaria` ;

CREATE TABLE IF NOT EXISTS `siscoord`.`Professor_has_Portaria` (
  `idProfessorHasPortaria` INT NOT NULL AUTO_INCREMENT,
  `Professor_idProfessor` INT NOT NULL,
  `Portaria_idPortaria` INT NOT NULL,
  INDEX `fk_Professor_has_Portaria_Portaria1_idx` (`Portaria_idPortaria` ASC),
  INDEX `fk_Professor_has_Portaria_Professor1_idx` (`Professor_idProfessor` ASC),
  PRIMARY KEY (`idProfessorHasPortaria`),
  CONSTRAINT `fk_Professor_has_Portaria_Professor1`
    FOREIGN KEY (`Professor_idProfessor`)
    REFERENCES `siscoord`.`Professor` (`idProfessor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Professor_has_Portaria_Portaria1`
    FOREIGN KEY (`Portaria_idPortaria`)
    REFERENCES `siscoord`.`Portaria` (`idPortaria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `siscoord`.`Curso`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `siscoord`.`Curso` ;

CREATE TABLE IF NOT EXISTS `siscoord`.`Curso` (
  `idCurso` INT NOT NULL AUTO_INCREMENT,
  `nmCurso` VARCHAR(45) NOT NULL,
  `siglaCursoSuap` VARCHAR(45) NOT NULL,
  `coordenador` INT NOT NULL,
  PRIMARY KEY (`idCurso`),
  INDEX `fk_Curso_Professor_has_Portaria1_idx` (`coordenador` ASC),
  CONSTRAINT `fk_Curso_Professor_has_Portaria1`
    FOREIGN KEY (`coordenador`)
    REFERENCES `siscoord`.`Professor_has_Portaria` (`idProfessorHasPortaria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `siscoord`.`Discente_has_Curso`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `siscoord`.`Discente_has_Curso` ;

CREATE TABLE IF NOT EXISTS `siscoord`.`Discente_has_Curso` (
  `Discente_idDiscente` INT NOT NULL,
  `Curso_idCurso` INT NOT NULL,
  `matricula` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`Discente_idDiscente`, `Curso_idCurso`),
  INDEX `fk_Discente_has_Curso_Curso1_idx` (`Curso_idCurso` ASC),
  INDEX `fk_Discente_has_Curso_Discente1_idx` (`Discente_idDiscente` ASC),
  CONSTRAINT `fk_Discente_has_Curso_Discente1`
    FOREIGN KEY (`Discente_idDiscente`)
    REFERENCES `siscoord`.`Discente` (`idDiscente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Discente_has_Curso_Curso1`
    FOREIGN KEY (`Curso_idCurso`)
    REFERENCES `siscoord`.`Curso` (`idCurso`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `siscoord`.`Professor_has_Curso`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `siscoord`.`Professor_has_Curso` ;

CREATE TABLE IF NOT EXISTS `siscoord`.`Professor_has_Curso` (
  `Professor_idProfessor` INT NOT NULL,
  `Curso_idCurso` INT NOT NULL,
  PRIMARY KEY (`Professor_idProfessor`, `Curso_idCurso`),
  INDEX `fk_Professor_has_Curso_Curso1_idx` (`Curso_idCurso` ASC),
  INDEX `fk_Professor_has_Curso_Professor1_idx` (`Professor_idProfessor` ASC),
  CONSTRAINT `fk_Professor_has_Curso_Professor1`
    FOREIGN KEY (`Professor_idProfessor`)
    REFERENCES `siscoord`.`Professor` (`idProfessor`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_Professor_has_Curso_Curso1`
    FOREIGN KEY (`Curso_idCurso`)
    REFERENCES `siscoord`.`Curso` (`idCurso`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `siscoord`.`TAE`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `siscoord`.`TAE` ;

CREATE TABLE IF NOT EXISTS `siscoord`.`TAE` (
  `idTAE` INT NOT NULL AUTO_INCREMENT,
  `idPessoa` INT NOT NULL,
  `matricula` VARCHAR(45) NOT NULL,
  `user` VARCHAR(400) NOT NULL,
  `passWd` VARCHAR(400) NULL,
  PRIMARY KEY (`idTAE`),
  UNIQUE INDEX `user_UNIQUE` (`user` ASC),
  UNIQUE INDEX `passWd_UNIQUE` (`passWd` ASC),
  INDEX `fk_TAE_Pessoa1_idx` (`idPessoa` ASC),
  CONSTRAINT `fk_TAE_Pessoa1`
    FOREIGN KEY (`idPessoa`)
    REFERENCES `siscoord`.`Pessoa` (`idPessoa`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `siscoord`.`Externo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `siscoord`.`Externo` ;

CREATE TABLE IF NOT EXISTS `siscoord`.`Externo` (
  `idExterno` INT NOT NULL AUTO_INCREMENT,
  `Pessoa_idPessoa` INT NOT NULL,
  `endereco` VARCHAR(200) NULL,
  `telefone` VARCHAR(45) NULL,
  PRIMARY KEY (`idExterno`),
  INDEX `fk_Externo_Pessoa1_idx` (`Pessoa_idPessoa` ASC),
  CONSTRAINT `fk_Externo_Pessoa1`
    FOREIGN KEY (`Pessoa_idPessoa`)
    REFERENCES `siscoord`.`Pessoa` (`idPessoa`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `siscoord`.`TAE_has_Portaria`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `siscoord`.`TAE_has_Portaria` ;

CREATE TABLE IF NOT EXISTS `siscoord`.`TAE_has_Portaria` (
  `TAE_idTAE` INT NOT NULL,
  `Portaria_idPortaria` INT NOT NULL,
  PRIMARY KEY (`TAE_idTAE`, `Portaria_idPortaria`),
  INDEX `fk_TAE_has_Portaria_Portaria1_idx` (`Portaria_idPortaria` ASC),
  INDEX `fk_TAE_has_Portaria_TAE1_idx` (`TAE_idTAE` ASC),
  CONSTRAINT `fk_TAE_has_Portaria_TAE1`
    FOREIGN KEY (`TAE_idTAE`)
    REFERENCES `siscoord`.`TAE` (`idTAE`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_TAE_has_Portaria_Portaria1`
    FOREIGN KEY (`Portaria_idPortaria`)
    REFERENCES `siscoord`.`Portaria` (`idPortaria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `siscoord`.`Setor`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `siscoord`.`Setor` ;

CREATE TABLE IF NOT EXISTS `siscoord`.`Setor` (
  `idSetor` INT NOT NULL AUTO_INCREMENT,
  `nmSetor` VARCHAR(45) NOT NULL,
  `siglaSetorSuap` VARCHAR(45) NOT NULL,
  `idCoordenador` INT NULL,
  `tpCoordenador` ENUM('Professor', 'TAE') NULL,
  PRIMARY KEY (`idSetor`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `siscoord`.`TAE_has_Setor`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `siscoord`.`TAE_has_Setor` ;

CREATE TABLE IF NOT EXISTS `siscoord`.`TAE_has_Setor` (
  `TAE_idTAE` INT NOT NULL,
  `Setor_idSetor` INT NOT NULL,
  PRIMARY KEY (`TAE_idTAE`, `Setor_idSetor`),
  INDEX `fk_TAE_has_Setor_Setor1_idx` (`Setor_idSetor` ASC),
  INDEX `fk_TAE_has_Setor_TAE1_idx` (`TAE_idTAE` ASC),
  CONSTRAINT `fk_TAE_has_Setor_TAE1`
    FOREIGN KEY (`TAE_idTAE`)
    REFERENCES `siscoord`.`TAE` (`idTAE`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_TAE_has_Setor_Setor1`
    FOREIGN KEY (`Setor_idSetor`)
    REFERENCES `siscoord`.`Setor` (`idSetor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



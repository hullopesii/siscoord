use siscoord;

INSERT INTO `Pessoa` (`idPessoa`, `nmPessoa`, `cpf`) VALUES ('1', 'Bruno', '12345678910');

INSERT INTO `Professor` (`idProfessor`, `idPessoa`, `matricula`, `area`, `user`, `passWd`) VALUES ('1', '1', '123456', 'informatica', 'bruno', 'bruno');

INSERT INTO `Portaria` (`idPortaria`, `nroPortaria`, `src`, `tipo`) VALUES
(1, '01/2017', NULL, NULL);

INSERT INTO `Professor_has_Portaria` (`idProfessorHasPortaria`, `Professor_idProfessor`, `Portaria_idPortaria`) VALUES (NULL, '1', '1');

INSERT INTO `Curso` (`idCurso`, `nmCurso`, `siglaCursoSuap`, `coordenador`) VALUES (NULL, 'Sistemas de informacao', 'CBSI', '1');


INSERT INTO `Setor` (`idSetor`, `nmSetor`, `siglaSetorSuap`, `idCoordenador`, `tpCoordenador`) VALUES (NULL, 'GEPEX', 'GEPEX', '1', 'Professor');

INSERT INTO `Pessoa` (`idPessoa`, `nmPessoa`, `cpf`) VALUES (2, 'Josefino', '5555');

INSERT INTO `Portaria` (`idPortaria`, `nroPortaria`, `src`, `tipo`) VALUES (2, '02/2017', 'teste', 'portaria');

INSERT INTO `Setor` (`idSetor`, `nmSetor`, `siglaSetorSuap`, `idCoordenador`, `tpCoordenador`) VALUES (NULL, 'CORAE', 'CORAE', '2', 'TAE');


package br.tatuapu.view;

/**
 * @author R�ben Fran�a
 */

import javax.swing.*;

import com.mysql.fabric.xmlrpc.base.Array;

import java.util.ArrayList;
import java.util.List;

import br.tatuapu.model.*;
import br.tatuapu.util.Forms;
import br.tatuapu.util.Mensageiro;

import static br.tatuapu.util.Contexto.*;

import java.awt.*;
/*
 * @author Rúben França 
 */
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class JanelaProfessor extends JFrame {
	private static final long serialVersionUID = 1L;

	JTextField tId;
	JTextField tNome;
	JTextField tCpf;
	JTextField tArea;
	JTextField tMatricula;
	JTextField tUser;
	JTextField tPassWd;
	JTable tabelaProfessor;
	ProfessorDAO professorBanco;
	Professor prof = null;
	PessoaDAO pessoaBanco;

	private JPanel painelSuperior;

	private static final String NMMODULO = "Cadastro de Professor";

	public JanelaProfessor () {
		super(APLICACAONOME + " - " + APLICACAOVERSAO + " - Cadastro de Professores");
		setSize(APLICACAOWIDTH,APLICACAOHEIGHT);

		setDefaultCloseOperation(EXIT_ON_CLOSE);

		//Centralizar
		setLocationRelativeTo(null);

		JPanel painel = new JPanel(new BorderLayout());


		/*
		 * Criando as Labels e os Text Fields
		 */
		painelSuperior = new JPanel(new GridLayout(6, 2, 0, 0));


		//Nome
		JLabel lNome = new JLabel("Nome: ");
		painelSuperior.add(lNome);
		tNome = new JTextField(30);
		painelSuperior.add(tNome);

		//CPF
		JLabel lCpf = new JLabel("CPF: ");
		painelSuperior.add(lCpf);
		tCpf = new JTextField(12);
		painelSuperior.add(tCpf);

		//Area
		JLabel lArea = new JLabel("Area: ");
		painelSuperior.add(lArea);
		tArea = new JTextField(10);
		painelSuperior.add(tArea);

		//Matricula
		JLabel lMatricula = new JLabel("Matricula: ");
		painelSuperior.add(lMatricula);
		tMatricula = new JTextField(15);
		painelSuperior.add(tMatricula);

		painel.add("North", painelSuperior);

		/*
		 * Criando Botoes
		 */

		JToolBar barBotoes = new JToolBar();
		barBotoes.setFloatable(false);

		//Bot�o Apagar
		JButton bDeletar = new JButton("Apagar");
		barBotoes.add(bDeletar);

		//Bot�o Limpar
		JButton bLimpar = new JButton("Limpar");
		barBotoes.add(bLimpar);

		//Bot�o Adicionar
		JButton bAdd = new JButton("Adicionar");
		barBotoes.add(bAdd);



		JPanel painelCentral = new JPanel(new BorderLayout());

		//Adicionando os botoes no painel
		painelCentral.add("North",barBotoes);

		tabelaProfessor = new JTable(null);
		ProfessorTableModel modelo = new ProfessorTableModel();
		tabelaProfessor.setModel(modelo);

		JScrollPane barraRolagem = new JScrollPane(tabelaProfessor);

		painelCentral.add("Center",barraRolagem);
		painel.add("Center",painelCentral);

		setContentPane(painel);

		/*
		 * Criando A��es dos botões
		 */

		bDeletar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					bDeletarAction();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});

		bLimpar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				bLimparAction();

			}
		});

		/*
		 * Adicionando a��o ao click do mouse na tabela
		 */

		bAdd.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					bAddAction();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

		tabelaProfessor.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub

				if(e.getClickCount()>1)
					preencheCampos();

			}


		});

		tCpf.addFocusListener(new FocusListener() {
			/*
			 * (non-Javadoc)
			 * @see java.awt.event.FocusListener#focusLost(java.awt.event.FocusEvent)
			 * M�todo para detectar perca de foco do text field.
			 */
			@Override
			public void focusLost(FocusEvent e) {
				List pessoas;
				String[] pessoaCheck = {"0","",tCpf.getText()};

				try {
					pessoaBanco = new PessoaDAO();
				
					pessoas = pessoaBanco.procura(pessoaCheck);
					
					if (!pessoas.isEmpty())
						preencheDados(pessoas);	
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}


			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub

			}
		});

		try {
			carregarDados();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}	

	public void bDeletarAction() throws Exception {

		ProfessorTableModel modelo = (ProfessorTableModel) tabelaProfessor.getModel();
		int id = tabelaProfessor.getSelectedRow();
		Professor professor = modelo.getRowAt(id);

		try {
			professorBanco.excluir(professor);
			modelo.removeRow(tabelaProfessor.getSelectedRow());
			JOptionPane.showMessageDialog(null, "Deletado com sucesso");
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}

	}

	public void bLimparAction() {
		limpaTela();
	}

	public void bAddAction() throws Exception {
		if (prof == null) {
			System.out.println("1");
			PessoaDAO bdPessoa = new PessoaDAO();

			String[] pessoa = {tNome.getText(),tCpf.getText()};
			bdPessoa.salvar(pessoa);

			Professor p = new Professor(bdPessoa.getLastID(), null, tCpf.getText() , tArea.getText(), tMatricula.getText(), tCpf.getText() , "123456" , '0');
			professorBanco = new ProfessorDAO();
			professorBanco.salvar(p);

			ProfessorTableModel modelP = (ProfessorTableModel) tabelaProfessor.getModel();
			modelP.addRow(new Professor (bdPessoa.getLastID(), tNome.getText(), tCpf.getText() , tArea.getText(), tMatricula.getText(), tCpf.getText() , "123456" , '0'));
			Mensageiro.mostraMensagemSucesso("novo", NMMODULO);
			limpaTela();
		} else {
			System.out.println("2");
			PessoaDAO bdPessoa = new PessoaDAO();
			//professorBanco = new ProfessorDAO();
			ProfessorTableModel modelP = (ProfessorTableModel)tabelaProfessor.getModel();

			int id = tabelaProfessor.getSelectedRow();
			prof = modelP.getRowAt(id);

			Professor p = new Professor(prof.getIdPessoa(), tNome.getText(), tCpf.getText() , 
					tArea.getText(), tMatricula.getText(), tCpf.getText() , "123456" , prof.getIdProfessor());

			professorBanco.atualizar(p);


			modelP.removeRow(tabelaProfessor.getSelectedRow());
			//Instancia-se de novo o professor pois após a atualização da tabela, sem a instancia fica sem nome.
			modelP.addRow(new Professor (prof.getIdPessoa(), tNome.getText(), tCpf.getText() , tArea.getText(), tMatricula.getText(), tCpf.getText() , "123456" , prof.getIdProfessor()));
			Mensageiro.mostraMensagemSucesso("Alteração feita com sucesso", NMMODULO );

		}
	}

	private void limpaTela() {
		Forms.clearTextFields(painelSuperior);
	}

	private void carregarDados() throws Exception {
		List<Professor> lista;
		professorBanco = new ProfessorDAO();
		lista = professorBanco.listaTodos();
		ProfessorTableModel pModel = new ProfessorTableModel();

		for(Professor p : lista) {
			pModel.addRow(p);
		}

		tabelaProfessor.setModel(pModel);
	}

	/*
	 * M�todo que preenche os campos do tipo JTextFiel da classe JanelaProfessor para altera��o dos dados
	 */
	private void preencheCampos() {


		ProfessorTableModel modelP = (ProfessorTableModel)tabelaProfessor.getModel();
		int id = tabelaProfessor.getSelectedRow();

		prof = modelP.getRowAt(id);

		tNome.setText(prof.getNmPessoa());
		tCpf.setText(prof.getCpf());
		tArea.setText(prof.getArea());
		tMatricula.setText(prof.getMatricula());

	}

	/*
	 * M�todo que recebe um arraylist caso haja pessoa com aquele cpf e vai preencher 
	 * o campo nome.
	 */
	private void preencheDados(List pessoas) { 
		tNome.setText(((Pessoa) pessoas.get(0)).getNmPessoa());

	}
}

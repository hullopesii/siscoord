package br.tatuapu.view;
/* Bruno Barbosa Franca 

 * */

import javax.swing.*;
import javax.swing.plaf.ActionMapUIResource;

import br.tatuapu.model.*;
import br.tatuapu.util.Forms;
import br.tatuapu.util.Mensageiro;

import static br.tatuapu.util.Contexto.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;

public class JanelaCurso extends JFrame{
	private static final String NMMODULO = "Cursos";
	private JTextField tNmCurso;
	private JTextField tSigla;
	private JTable cTabela;
	private CursoDAO cursoBanco;
	private JPanel painelSuperior;
	private Curso cursoSelecionado=null;
	private CoordenadorDAO coordenadorBanco;
	private JComboBox cCoordenador;


	public JanelaCurso() {
		super(APLICACAONOME + " - " + APLICACAOVERSAO);
		setSize(APLICACAOWIDTH , APLICACAOHEIGHT);
		//centralizar
		setLocationRelativeTo(null);
		JPanel painel = new JPanel(new BorderLayout());
		JToolBar bar = new JToolBar();

		//criacao dos botoes
		JButton bSalvar = new JButton ("Salvar");
		bar.add("North",bSalvar);
		JButton bLimpar = new JButton ("Limpar");
		bar.add("Center",bLimpar);
		JButton bDeletar = new JButton ("Deletar");
		bar.add("South",bDeletar);
		bar.setFloatable(false);

		//label e text fields
		painelSuperior = new JPanel(new GridLayout(3,6, 0, 0));

		JLabel lNmCurso = new JLabel("Nome Curso");
		tNmCurso = new JTextField();
		painelSuperior.add(lNmCurso);
		painelSuperior.add(tNmCurso);

		JLabel lSigla = new JLabel("Sigla");
		tSigla = new JTextField();
		painelSuperior.add(lSigla);
		painelSuperior.add(tSigla);

		JLabel lCoordenador = new JLabel("Coordenador");
		painelSuperior.add(lCoordenador);

		//combobox de coordenadores		
		cCoordenador = new JComboBox();
		preencheComboBox();

		painelSuperior.add(cCoordenador);

		//tabela
		cTabela = new JTable();
		JScrollPane barraRolagem = new JScrollPane(cTabela);
		JPanel painelCentral = new JPanel(new BorderLayout());
		CursoTableModel cursoModel = new CursoTableModel();
		cTabela.setModel(cursoModel);
		painelCentral.add("Center",barraRolagem);
		painelCentral.add("North",bar);
		buscarDados();		
		painel.add("Center", painelCentral);	
		painel.add("North",painelSuperior);

		//adicionando ações aos botoes
		bSalvar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				salvarDados();

			}
		});
		bDeletar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				excluirDados();

			}
		});
		bLimpar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				limparDados();

			}


		});
		cTabela.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getClickCount()>1) {
					preencheCampos();
				}

			}
		});

		setContentPane(painel);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);



	}
	//metodos
	public void salvarDados() {
		Professor prof = (Professor) cCoordenador.getSelectedItem();
		if(cursoSelecionado == null) {
			if(!tNmCurso.equals("")||!tSigla.equals("")||cCoordenador.getSelectedIndex()==-1){
				Curso curso = new Curso(0,tNmCurso.getText(),tSigla.getText(),prof.getIdProfessor());
				CursoDAO conCurso;
				CursoTableModel modelo = (CursoTableModel) cTabela.getModel();

				try {
					conCurso = new CursoDAO();
					conCurso.salvar(curso);
					modelo.addRow(new Curso(conCurso.getLastID(),tNmCurso.getText(), tSigla.getText(),prof.getIdProfessor()));
					Mensageiro.mostraMensagemSucesso("novo", NMMODULO);
					limparDados();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}else{
				JOptionPane.showMessageDialog(null, "Existem campos em branco!");
			}
		}else {
			Curso curso = new Curso(cursoSelecionado.getIdCurso(),tNmCurso.getText(), tSigla.getText(),prof.getIdProfessor());
			salvarDadosAlterados(curso);
			CursoTableModel modelo = (CursoTableModel) cTabela.getModel();
			modelo.removeRow(cTabela.getSelectedRow());
			modelo.addRow(curso);
			Mensageiro.mostraMensagemSucesso("alteração", NMMODULO);
			limparDados();
		}
	}
	public void excluirDados()  {
		CursoTableModel modelo = (CursoTableModel) cTabela.getModel();
		int id = cTabela.getSelectedRow();
		Curso curso = modelo.getRowAt(id);

		try {
			cursoBanco.excluir(curso);
			modelo.removeRow(cTabela.getSelectedRow());
			JOptionPane.showMessageDialog(null, "Deletado com sucesso");
			limparDados();
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	public void buscarDados() {
		try {
			List<Curso> lista;
			cursoBanco = new CursoDAO();
			lista = cursoBanco.listaTodos();
			CursoTableModel cModel = new CursoTableModel();

			for(Curso c : lista) {

				cModel.addRow(c);

			}
			cTabela.setModel(cModel);
		}catch(Exception e) {
			JOptionPane.showMessageDialog(null, "problema ao carregar dados!"+e.getMessage());
		}
	}
	@SuppressWarnings("unchecked")
	public void preencheComboBox() {
		try {
			List<Professor> lista;
			coordenadorBanco = new CoordenadorDAO();
			lista = coordenadorBanco.listaTodos();
			CoordenadorComboBoxModel modelo = new CoordenadorComboBoxModel();
			for(Professor p: lista) {
				modelo.addItem(p);
			}
			cCoordenador.setModel(modelo);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "problema ao carregar dados2!"+e.getMessage());
		}
	}
	public void limparDados() {
		Forms.clearTextFields(painelSuperior);
		cursoSelecionado=null;
	}
	public void salvarDadosAlterados(Curso curso) {
		try {
			cursoBanco.atualizar(curso);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	public void preencheCampos() {
		CursoTableModel modelo = (CursoTableModel) cTabela.getModel();
		int id = cTabela.getSelectedRow();
		cursoSelecionado = modelo.getRowAt(id);
		tNmCurso.setText(cursoSelecionado.getNmCurso());
		tSigla.setText(cursoSelecionado.getSiglaCursoSuap());
	}



}

package br.tatuapu.view;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;

import br.tatuapu.util.Forms;
import br.tatuapu.util.Mensageiro;
import br.tatuapu.*;
import br.tatuapu.model.*;
import static br.tatuapu.util.Contexto.*;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

public class JanelaAssociaPortaria extends JFrame {

	private static final String NMMODULO = "Informação";
	JComboBox<Professor> boxProfessor;
	JComboBox<TAE> boxTAE;
	JTextField tPortaria;
	JTable tabelaPortaria;
	JPanel painelSuperior;
	TAEComboBoxModel modeloTAE;
	PortariaDAO portDao;
	
	
	public JanelaAssociaPortaria() throws Exception {
		
		super(APLICACAONOME + " - " + APLICACAOVERSAO + " - Associa Portaria");
		setSize(APLICACAOWIDTH, APLICACAOHEIGHT);
		setLocationRelativeTo(null);

		JPanel painel = new JPanel();
		JToolBar bar = new JToolBar();
		bar.setFloatable(false);
		
		painelSuperior = new JPanel(new GridLayout(2, 3, 0, 0));
		

		painel.setLayout(new BorderLayout());
		
		JLabel lProfessor = new JLabel("Professor:");
		painelSuperior.add(lProfessor);
		
		boxProfessor = new JComboBox<>();
		ProfessorComboBoxModel modeloProf = new ProfessorComboBoxModel();
		boxProfessor.setModel(modeloProf);
		painelSuperior.add(boxProfessor);
		preencheComboBoxProfessor();
		
		JButton bAssociarProf = new JButton("Associar");
		painelSuperior.add(bAssociarProf);
		
		tPortaria = new JTextField(20);
		painelSuperior.add(tPortaria);
		
		JLabel lTAE = new JLabel("TAE:");
		painelSuperior.add(lTAE);
		
		boxTAE = new JComboBox<TAE>();
		TAEComboBoxModel modeloTAE =new TAEComboBoxModel();
		boxTAE.setModel(modeloTAE);
		painelSuperior.add(boxTAE);
		preencheComboBoxTAE();
		
		JButton bAssociarPort = new JButton("Associar");
		painelSuperior.add(bAssociarPort);
		
		painel.add("North", painelSuperior);
		
		JPanel painelCentral = new JPanel(new BorderLayout());
		painelCentral.add("North", bar);

		tabelaPortaria = new JTable();
		PortariaTableModel modelo = new PortariaTableModel();
		tabelaPortaria.setModel(modelo);

		JScrollPane barraRolagem = new JScrollPane(tabelaPortaria);
		painelCentral.add("Center", barraRolagem);

		painel.add("Center", painelCentral);
		
		setContentPane(painel);
		
		bAssociarProf.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				try {
					bAssociarProfAction();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Mensageiro.mostraMensagemErro("remocao", NMMODULO, e.getMessage());
				}
			}
		});
		
		bAssociarPort.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				try {
					bAssociarTAEAction();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Mensageiro.mostraMensagemErro("remocao", NMMODULO, e.getMessage());
				}
			}
		});
		
		tabelaPortaria.addMouseListener(new java.awt.event.MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() > 1)
				preencheCampos();

			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

		});
		
		carregaDados();
		
	}
	
	public void preencheComboBoxProfessor()  {
		
		try{
			
		List<Professor> lista;
		ProfessorDAO prof = new ProfessorDAO();
		lista = prof.listaTodos();
		ProfessorComboBoxModel modelo = new ProfessorComboBoxModel();
		
		for(Professor p:lista) {
			
			modelo.addItem(p);
		}
		
		boxProfessor.setModel(modelo);
		
		}
		
		catch(Exception e) {
			
			JOptionPane.showMessageDialog(null, "Problema ao carregar");
		}
	}
	
	public void preencheComboBoxTAE() {
		
		try{
			
			List<TAE> lista;
			TAEDAO tae = new TAEDAO();
			lista = tae.listaTodos();
			TAEComboBoxModel modelo = new TAEComboBoxModel();
			
			for(TAE p:lista) {
				
				modelo.addItem(p);
			}
			
			boxTAE.setModel(modelo);
			
			}
			
			catch(Exception e) {
				
				JOptionPane.showMessageDialog(null, "Problema ao carregar");
			}
	}
	
	public void carregaDados() throws Exception {

		PortariaTableModel modelo = new PortariaTableModel();
		PortariaDAO portDao = new PortariaDAO();
		List<Portaria> dados = (List<Portaria>) portDao.listaTodos();

		for (Portaria p : dados) {

			modelo.addRow(p);
		}

		tabelaPortaria.setModel(modelo);

	}
	
	public void preencheCampos() {

		try {

			portDao = new PortariaDAO();
		}

		catch (Exception e) {

			e.printStackTrace();
		}

		PortariaTableModel modelo = (PortariaTableModel) tabelaPortaria.getModel();
		int linhaSelecionada = tabelaPortaria.getSelectedRow();
		Portaria portariaSelecionada = modelo.getRowAt(linhaSelecionada);

		tPortaria.setText(portariaSelecionada.getNroPortaria());
		

	}
	
	public void bAssociarProfAction() throws Exception {
		
		try{
			
         AssociaPortariaProfessorDAO assDao = new AssociaPortariaProfessorDAO();
		
		 PortariaTableModel modelo = (PortariaTableModel)tabelaPortaria.getModel();
		 int linhaSelecionada = tabelaPortaria.getSelectedRow();
		 Portaria portariaSelecionada = modelo.getRowAt(linhaSelecionada);
		
		 Professor professorSelecionado = (Professor) boxProfessor.getSelectedItem();
		
		 assDao.associaPortariaProfessor(portariaSelecionada.getIdPortaria(), professorSelecionado.getIdProfessor());
		 tabelaPortaria.getSelectionModel().clearSelection();
		 Mensageiro.mostraMensagemSucesso("novo", NMMODULO);
		
		  }
		
		catch(IndexOutOfBoundsException e ) {
			
			JOptionPane.showMessageDialog(null, "Selecione uma Portaria e Um Professor por Favor");
		}

	}
	
	public void bAssociarTAEAction() throws Exception {
		
		try{
			
	      AssociaPortariaTAEDAO assDao = new AssociaPortariaTAEDAO();
			
	      PortariaTableModel modelo = (PortariaTableModel)tabelaPortaria.getModel();
		  int linhaSelecionada = tabelaPortaria.getSelectedRow();
		  Portaria portariaSelecionada = modelo.getRowAt(linhaSelecionada);
			
		  TAE taeSelecionada = (TAE) boxTAE.getSelectedItem();
			
		  assDao.associaPortariaProfessor(portariaSelecionada.getIdPortaria(), taeSelecionada.getIdTAE());
		  tabelaPortaria.getSelectionModel().clearSelection();
		  Mensageiro.mostraMensagemSucesso("novo", NMMODULO);
			
		 }
			
			catch(IndexOutOfBoundsException e ) {
				
				JOptionPane.showMessageDialog(null, "Selecione uma Portaria e Uma TAE por Favor");
			}
	 }
	
		
	public static void main(String[] args) throws Exception {
			
		JanelaAssociaPortaria j =new JanelaAssociaPortaria();
		j.setVisible(true);
	  }
			
	}
	


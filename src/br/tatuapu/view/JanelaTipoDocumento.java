package br.tatuapu.view;

import javax.swing.*;

import br.tatuapu.model.*;
import br.tatuapu.util.*;

import static br.tatuapu.util.Contexto.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

public class JanelaTipoDocumento extends JFrame implements JanelaPadraoComTabela{
	
	private static final String NMMODULO = "Tipos de Documentos";
	private JTextField tTipoDocumentoNome;
	private JTextField tTipoDocumentoDesc;
	private JTable tabela;
	JPanel painelSuperior;
	private ArrayList camposObrigatorios;
	private TipoDocumento tipoDocumentoCarregado;
	private int linhaTipoDocumentoCarregado;

	public JanelaTipoDocumento(){
		super(APLICACAONOME + " - " + APLICACAOVERSAO + " - Cadastro de Tipos de Documentos");
		setSize(APLICACAOWIDTH,APLICACAOHEIGHT);
		//centralizando
		setLocationRelativeTo(null);
		
		camposObrigatorios = new ArrayList();
		
		JPanel painel = new JPanel(new BorderLayout());
		JToolBar bar = new JToolBar();
		bar.setFloatable(false);
		
		//painel do formulário
		painelSuperior = new JPanel(new GridLayout(2,2,0,0));		
			
		//criando a estrutura da janela
		JLabel lTipoDocumentoNome = new JLabel("Nome do Tipo de Documento: ");
		painelSuperior.add(lTipoDocumentoNome);
		
		tTipoDocumentoNome = new JTextField(15);
		tTipoDocumentoNome.setToolTipText("Nome Completo");
		camposObrigatorios.add(tTipoDocumentoNome);//setando como obrigatório
		painelSuperior.add(tTipoDocumentoNome);
		
		JLabel lTipoDocumentoDesc = new JLabel("Descrição: ");
		painelSuperior.add(lTipoDocumentoDesc);
		
		tTipoDocumentoDesc = new JTextField(15);
		painelSuperior.add(tTipoDocumentoDesc);
		
		painel.add("North", painelSuperior);
		
		JPanel painelCentral = new JPanel(new BorderLayout());
		painelCentral.add("North",bar);
		
		
		
		tabela = new JTable();
		TipoDocumentoTableModel modelo = new TipoDocumentoTableModel();
		
		JScrollPane barraRolagem = new JScrollPane(tabela);
		
		painelCentral.add("Center", barraRolagem);
		
		painel.add("Center",painelCentral);
		
		//botões
		JButton bDeletar = new JButton("Deletar");
		bar.add(bDeletar);
		
		JButton bSalvar = new JButton("Salvar");
		bar.add(bSalvar);
		
		JButton bLimpar = new JButton("Limpar");
		bar.add(bLimpar);
		
		setContentPane(painel);
		
		//ações dos botões
		bSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMaisActionPerformed();
            }
        });
		
		bLimpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimparActionPerformed();
            }
        });
		
		bDeletar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeletarActionPerformed();
            }
        });
		
		//ação da tabela
		tabela.addMouseListener(new java.awt.event.MouseListener(){

			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getClickCount()>1)
					preencheCampos();
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		preencheTabela();
		
		//popup menu
        //Add listener to components that can bring up popup menus.
        MouseListener popupListener = new PopupListener(this);
        tabela.addMouseListener(popupListener);
	}
	
	@Override
	public void preencheCampos(){
		int id = tabela.getSelectedRow();
        //garantindo que selecionou-se um item da tabela
        if(id>=0){
        	TipoDocumentoTableModel modelo = (TipoDocumentoTableModel) tabela.getModel();
        	TipoDocumento tpDocumento = modelo.getRowAt(id);
        	tTipoDocumentoNome.setText(tpDocumento.getTipoDocumentoNome());
        	tTipoDocumentoDesc.setText(tpDocumento.getTipoDocumentoDesc());
        	tipoDocumentoCarregado = tpDocumento;
        	linhaTipoDocumentoCarregado = id;
        }
	}
	@Override
	public void btnDeletarActionPerformed(){
		int id = tabela.getSelectedRow();
        //garantindo que selecionou-se um item da tabela
        if(id>=0){
            TipoDocumentoTableModel modelo = (TipoDocumentoTableModel) tabela.getModel();
            TipoDocumento tpDocumento = modelo.getRowAt(id);
//            
            int opcao = Mensageiro.Confirm("Deseja Realmente Remover este Registro?");
            if (opcao == 0 || opcao == -1) {
            	TipoDocumentoDAO dao = new TipoDocumentoDAO();
            	try {
					dao.excluir(tpDocumento);
					modelo.removeRow(id);
				} catch (Exception e) {
					Mensageiro.mostraMensagemErro("remoção", NMMODULO, e.getMessage());
				}
            }
        }else{
        	Mensageiro.mostraMensagemGenerica("Selecione um registro na tabela para editar!");
        }
	}
	
	@Override
	public void btnLimparActionPerformed(){
		Forms.clearTextFields(painelSuperior);
	}
	
	@Override
	public void preencheTabela() {
		TipoDocumentoDAO dao = new TipoDocumentoDAO();
		
		TipoDocumentoTableModel modelo = new TipoDocumentoTableModel();
		try{
			List<TipoDocumento> lista;
			lista = dao.listaTodos();
			for(TipoDocumento td:lista){
				modelo.addRow(td);
			}
			tabela.setModel(modelo);
			Forms.ajustaColunasTabela(tabela);
		}catch(Exception e){
			Mensageiro.mostraMensagemErro("listar", NMMODULO, e.getMessage());
		}
		
		
	}

	@Override
	public void btnMaisActionPerformed(){
		TipoDocumentoDAO dao = new TipoDocumentoDAO();
		String nmTpDoc = tTipoDocumentoNome.getText();
		String dsTpDoc = tTipoDocumentoDesc.getText();	
		
		if (tipoDocumentoCarregado == null){
			if (podeSalvarFormulario()){				
				try {
					dao.salvar(new TipoDocumento(0, nmTpDoc, dsTpDoc));
					TipoDocumentoTableModel modelo = (TipoDocumentoTableModel) tabela.getModel();
					modelo.addRow(new TipoDocumento(dao.getLastID(), nmTpDoc, dsTpDoc));
					Mensageiro.mostraMensagemSucesso("novo", NMMODULO);
					limpaTela();
				} catch (Exception e) {
					Mensageiro.mostraMensagemErro("novo", NMMODULO, e.getMessage());
				}
			}
		}else{
			//bora salvar alteração
			if(podeSalvarFormulario()){
				try {
					dao.atualizar(new TipoDocumento(tipoDocumentoCarregado.getIdTipoDocumento(), nmTpDoc, dsTpDoc));
					TipoDocumentoTableModel modelo = (TipoDocumentoTableModel) tabela.getModel();
					modelo.atualizaLinha(linhaTipoDocumentoCarregado,new TipoDocumento(tipoDocumentoCarregado.getIdTipoDocumento(), nmTpDoc, dsTpDoc));
					
					Mensageiro.mostraMensagemSucesso("alteração", NMMODULO);
					limpaTela();
				} catch (Exception e) {
					Mensageiro.mostraMensagemErro("alteração", NMMODULO, e.getMessage());
				}
			}
		}
	}
	
	@Override
	public void limpaTela() {
		Forms.clearTextFields(painelSuperior);
		tipoDocumentoCarregado = null;
	}

	public boolean podeSalvarFormulario(){
		
		ArrayList camposVazios = Forms.checaCamposVazios(painelSuperior, camposObrigatorios);
		if(camposVazios.size()==0){
			return true;
		}else{
			for (int i=0;i<camposVazios.size();i++) {
				Component c = (Component) camposVazios.get(i);
				if(c instanceof JTextField){
					c.requestFocus();
					Mensageiro.mostraMensagemFormularioVazio(NMMODULO, null);
					return false;					
				}
			}
		}
		return true;
	}
	
	public static void main(String[] args){
		JanelaTipoDocumento j = new JanelaTipoDocumento();
		j.setVisible(true);
	}
	@Override
	public JTable getTabela(){
		return this.tabela;
	}
}

package br.tatuapu.view;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import br.tatuapu.model.Portaria;
import br.tatuapu.model.PortariaTableModel;
import br.tatuapu.util.Forms;
import br.tatuapu.util.Mensageiro;
import br.tatuapu.*;
import br.tatuapu.model.*;
import static br.tatuapu.util.Contexto.*;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

//Author: Ellias Lacerda

public class JanelaPortaria extends JFrame {

	private static final String NMMODULO = "Informação";
	JTextField tIdPortaria;
	JTextField tNroPortaria;
	JTextField tSrc;
	JTextField tTipo;
	JTable tabelaPortaria;
	JPanel painelSuperior;
	private ArrayList camposObrigatorios;
	PortariaDAO portDao;

	public JanelaPortaria() throws Exception {

		super(APLICACAONOME + " - " + APLICACAOVERSAO + " - Portaria");
		setSize(APLICACAOWIDTH, APLICACAOHEIGHT);
		setLocationRelativeTo(null);

		JPanel painel = new JPanel();
		JToolBar bar = new JToolBar();
		bar.setFloatable(false);

		// Painel do Formulario

		painelSuperior = new JPanel(new GridLayout(2, 2, 0, 0));
		painel.add("North", painelSuperior);

		painel.setLayout(new BorderLayout());

		// Criando a estrutura da janela

		JLabel lIdPortaria = new JLabel("ID Portaria: ");
		painelSuperior.add(lIdPortaria);

		tIdPortaria = new JTextField(15);
		painelSuperior.add(tIdPortaria);
		tIdPortaria.setEditable(false);

		JLabel lNroPortaria = new JLabel("Nro Portaria");
		painelSuperior.add(lNroPortaria);

		tNroPortaria = new JTextField(15);
		painelSuperior.add(tNroPortaria);

		JLabel lSrc = new JLabel("Tipo:");
		painelSuperior.add(lSrc);

		tSrc = new JTextField(15);
		painelSuperior.add(tSrc);

		JLabel lTipo = new JLabel("SRC:");
		painelSuperior.add(lTipo);

		tTipo = new JTextField(15);
		painelSuperior.add(tTipo);

		painel.add("North", painelSuperior);
		painel.add(bar);

		JPanel painelCentral = new JPanel(new BorderLayout());
		painelCentral.add("North", bar);

		tabelaPortaria = new JTable();
		PortariaTableModel modelo = new PortariaTableModel();
		tabelaPortaria.setModel(modelo);

		JScrollPane barraRolagem = new JScrollPane(tabelaPortaria);
		painelCentral.add("Center", barraRolagem);

		painel.add("Center", painelCentral);

		// Botoes

		JButton bDeletar = new JButton("Deletar");
		bar.add(bDeletar);

		JButton bSalvar = new JButton("Salvar");
		bar.add(bSalvar);

		JButton bLimpar = new JButton("Limpar");
		bar.add(bLimpar);

		setContentPane(painel);

		// Definindo eventos dos botoes

		bDeletar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				try {
					bDeletarAction();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Mensageiro.mostraMensagemErro("remocao", NMMODULO, e.getMessage());
				}
			}
		});

		bSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				try {
					bSalvarAction();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Mensageiro.mostraMensagemSucesso(NMMODULO, e.getMessage());
				}
			}
		});

		bLimpar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				bLimparAction();
			}
		});

		tabelaPortaria.addMouseListener(new java.awt.event.MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() > 1)
				preencheCampos();

			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

		});

		carregaDados();

	}

	public void bDeletarAction() throws Exception {

		int linha = tabelaPortaria.getSelectedRow();

		if (linha >= 0) {
			int coluna = tabelaPortaria.getSelectedColumn();
			PortariaDAO daoPortaria = new PortariaDAO();
			PortariaTableModel modelo = (PortariaTableModel) this.tabelaPortaria.getModel();
			Object id = (Integer) modelo.getValueAt(linha, coluna);

			try {

				daoPortaria.excluir(id);
				modelo.removeRow(tabelaPortaria.getSelectedRow());
				bLimparAction();
				Mensageiro.mostraMensagemSucesso("remocao", NMMODULO);
			}

			catch (Exception e) {
				Mensageiro.mostraMensagemErro("remocao", NMMODULO, e.getMessage());
              
				
				

			}

		}

		else {

			Mensageiro.mostraMensagemGenerica("Selecione uma linha para excluir!");
		}

	}

	public void bSalvarAction() throws Exception {

		if (tabelaPortaria.getSelectedRow()== -1) {

			try {
				
				if( !tNroPortaria.getText().isEmpty()
						&& !tSrc.getText().isEmpty() && !tTipo.getText().isEmpty()) {
					
				Portaria port = new Portaria(0, tNroPortaria.getText(), tSrc.getText(), tTipo.getText());
				PortariaDAO daoPortaria = new PortariaDAO();
				PortariaTableModel modelo = (PortariaTableModel) this.tabelaPortaria.getModel();
				daoPortaria.salvar(port);
				modelo.addRow(	new Portaria(daoPortaria.getLastID(), tNroPortaria.getText(), tSrc.getText(), tTipo.getText()));
				bLimparAction();
				Mensageiro.mostraMensagemSucesso("novo", NMMODULO);
				
				}
				
				else {
					
					Mensageiro.mostraMensagemErro("novo", NMMODULO, "Preencha todos os campos por favor!!");
				}

			}

			catch (Exception e) {

				Mensageiro.mostraMensagemErro("novo", NMMODULO, e.getMessage());
			}

		}
 
		else { 

			PortariaTableModel modelo = (PortariaTableModel)tabelaPortaria.getModel();
			int linhaSelecionada = tabelaPortaria.getSelectedRow();
			Portaria portariaSelecionada = modelo.getRowAt(linhaSelecionada);
			Portaria port = new Portaria(portariaSelecionada.getIdPortaria(), tNroPortaria.getText(), tSrc.getText(), tTipo.getText());
			portDao.atualizar(port);
			modelo.removeRow(tabelaPortaria.getSelectedRow());
			modelo.addRow(port);
			Mensageiro.mostraMensagemSucesso("alteracao", NMMODULO);
			bLimparAction();
			tabelaPortaria.getSelectionModel().clearSelection();
		}
	}

	public void bLimparAction() {

		Forms.clearTextFields(painelSuperior);

	}

	public void carregaDados() throws Exception {

		PortariaTableModel modelo = new PortariaTableModel();
		PortariaDAO portDao = new PortariaDAO();
		List<Portaria> dados = (List<Portaria>) portDao.listaTodos();

		for (Portaria p : dados) {

			modelo.addRow(p);
		}

		tabelaPortaria.setModel(modelo);

	}

	public void preencheCampos() {

		try {

			portDao = new PortariaDAO();
		}

		catch (Exception e) {

			e.printStackTrace();
		}

		PortariaTableModel modelo = (PortariaTableModel) tabelaPortaria.getModel();
		int linhaSelecionada = tabelaPortaria.getSelectedRow();
		Portaria portariaSelecionada = modelo.getRowAt(linhaSelecionada);

		tNroPortaria.setText(portariaSelecionada.getNroPortaria());
		tSrc.setText(portariaSelecionada.getTipo());
		tTipo.setText(portariaSelecionada.getSrc());

	}

	public boolean podeSalvarFormulario() {

		ArrayList camposVazios = Forms.checaCamposVazios(painelSuperior, camposObrigatorios);
		if (camposVazios.size() == 0) {
			return true;
		} else {
			for (int i = 0; i < camposVazios.size(); i++) {
				Component c = (Component) camposVazios.get(i);
				if (c instanceof JTextField) {
					c.requestFocus();
					Mensageiro.mostraMensagemFormularioVazio(NMMODULO, null);
					return false;
				}
			}
		}
		return true;
	}

	public static void main(String[] args) throws Exception {

		JanelaPortaria j = new JanelaPortaria();
		j.setVisible(true);
	}
}

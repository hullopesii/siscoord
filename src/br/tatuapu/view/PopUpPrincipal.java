/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.tatuapu.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.*;

import br.tatuapu.util.JanelaPadraoComTabela;

/**
 *
 * @author tatuapu
 */
public class PopUpPrincipal extends JPopupMenu {
    JMenuItem miDeletarRegistro;
    JanelaPadraoComTabela janela;
    
    public PopUpPrincipal(JanelaPadraoComTabela janela){
    	this.janela = janela;
        this.miDeletarRegistro =new JMenuItem("Deletar");
        add(miDeletarRegistro);
        this.miDeletarRegistro.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {                
                    removeSelecionado();
            }
        });
        
    }
    private void removeSelecionado(){
    	janela.btnDeletarActionPerformed();
    }
}

class PopupListener extends MouseAdapter {
	private JanelaPadraoComTabela janela;

	public PopupListener(JanelaPadraoComTabela janela){
		this.janela = janela;
	}
    public void mousePressed(MouseEvent e){
        if (e.isPopupTrigger())
            doPop(e);
    }

    public void mouseReleased(MouseEvent e){
        if (e.isPopupTrigger())
            doPop(e);
    }

    private void doPop(MouseEvent e){
        PopUpPrincipal menu = new PopUpPrincipal(janela);
        menu.show(e.getComponent(), e.getX(), e.getY());
    }
}
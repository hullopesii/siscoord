package br.tatuapu.view;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;

import br.tatuapu.model.Portaria;
import br.tatuapu.model.PortariaDAO;
import br.tatuapu.model.PortariaTableModel;
import br.tatuapu.model.Setor;
import br.tatuapu.model.SetorDAO;
import br.tatuapu.model.SetorTableModel;
import br.tatuapu.util.Forms;
import br.tatuapu.util.Mensageiro;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.List;

import static br.tatuapu.util.Contexto.*;
public class JanelaSetor extends JFrame {
	
	private static final String NMMODULO = "Setor";
	JTextField tNmSetor;
	JTextField tSiglaSetorSuap;
	JTable tabelaSetor;
	JPanel painelSuperior;
	SetorDAO setorBanco;
	boolean flag = false;
	
	public JanelaSetor() throws Exception {
		super(APLICACAONOME + " - " + APLICACAOVERSAO + " - Cadastro de Setores");
		setSize(APLICACAOWIDTH,APLICACAOHEIGHT);
		//centralizando
		setLocationRelativeTo(null);
		
		JPanel painel = new JPanel(new BorderLayout());
		JToolBar bar = new JToolBar();
		bar.setFloatable(false);
		
		//painel do formulário
		painelSuperior = new JPanel(new GridLayout(2,2,0,0));		
			
		//criando a estrutura da janela
		JLabel lNmSetor = new JLabel("Nome do Setor: ");
		painelSuperior.add(lNmSetor);
		
		tNmSetor = new JTextField(15);
		painelSuperior.add(tNmSetor);
		
		JLabel lSiglaSetorSuap = new JLabel("Descrição: ");
		painelSuperior.add(lSiglaSetorSuap);
		
		tSiglaSetorSuap = new JTextField(15);
		painelSuperior.add(tSiglaSetorSuap);
		
		painel.add("North", painelSuperior);
		
		JPanel painelCentral = new JPanel(new BorderLayout());
		painelCentral.add("North",bar);
		
		tabelaSetor = new JTable();
		SetorTableModel modelo = new SetorTableModel();
		tabelaSetor.setModel(modelo);
		
		JScrollPane barraRolagem = new JScrollPane(tabelaSetor);
		painelCentral.add("Center", barraRolagem);
		
		painel.add("Center",painelCentral);
		
		//botões
		JButton bDeletar = new JButton("Deletar");
		bar.add(bDeletar);
		
		JButton bSalvar = new JButton("Salvar");
		bar.add(bSalvar);
		
		JButton bLimpar = new JButton("Limpar");
		bar.add(bLimpar);
		
		setContentPane(painel);
		

		 //Definindo eventos dos botoes
		 
		 bDeletar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					bDeletarAction();
				}
			});
		 
		 bSalvar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					try {
						bSalvarAction();
					} catch (Exception e) {
						Mensageiro.mostraMensagemErro("novo", NMMODULO, e.getMessage());
					}
				}
			});
		 
		 bLimpar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					bLimparAction();
				}
			});
		 
		 tabelaSetor.addMouseListener(new java.awt.event.MouseListener(){

			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getClickCount()>1){
					flag = true;
					preencheCampos();
				}
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		}); 
		 
		 carregaDados();
	}
	
	/**
	 * Serve para remover um registro da tabela, persistindo no BD
	 */
	 public void bDeletarAction() {
		 try {
			this.setorBanco = new SetorDAO();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    	
    	 SetorTableModel modelo = (SetorTableModel) this.tabelaSetor.getModel();
    	 int linhaSelecionada = tabelaSetor.getSelectedRow();    	 
    	 
    	 if(linhaSelecionada >=0){
	    	 try {
	    		Setor setorSelecionado = modelo.getRowAt(linhaSelecionada);
	 			setorBanco.excluir(setorSelecionado);
	 			modelo.removeRow(tabelaSetor.getSelectedRow());
	 			JOptionPane.showMessageDialog(null, "Deletado com sucesso");
	 		}catch(Exception e) {
	 			Mensageiro.mostraMensagemErro("remoção", NMMODULO, e.getMessage());
	 		}
    	 }else{
    		 Mensageiro.mostraMensagemGenerica("Selecione um Setor para excluir!");
    	 }
     }
	 
     public void bSalvarAction() throws Exception {

    	if(tabelaSetor.getSelectedRow() == -1) {
    	 
    		try{
    			if(!tNmSetor.getText().isEmpty() && !tSiglaSetorSuap.getText().isEmpty()){
		    		Setor setor = new Setor(0, tNmSetor.getText(), tSiglaSetorSuap.getText()); 
			    	SetorDAO setorDAO = new SetorDAO(); 
			    	SetorTableModel modelo = (SetorTableModel) this.tabelaSetor.getModel();
			    	setorDAO.salvar(setor);
			    	modelo.addRow(new Setor(setorDAO.getLastID(), tNmSetor.getText(), tSiglaSetorSuap.getText()));		 
			    	bLimparAction();		  
			    	Mensageiro.mostraMensagemSucesso("novo", NMMODULO);
    			}
    			else{
    				Mensageiro.mostraMensagemErro("remoção",NMMODULO, "Campos Vazio");
    			}
    		}
    		
    		catch (Exception e){
    			
    			Mensageiro.mostraMensagemErro("novo", NMMODULO, e.getMessage());
    			
    		}
    		
    	}
    	
    	else{
    	 SetorTableModel modelo = (SetorTableModel) this.tabelaSetor.getModel();
       	 int linhaSelecionada = tabelaSetor.getSelectedRow();    	 
       	 
       	 try {
   	    		Setor setorSelecionado = modelo.getRowAt(linhaSelecionada);
   	    		Setor setor = new Setor(setorSelecionado.getIdSetor(), tNmSetor.getText(), tSiglaSetorSuap.getText()); 
   	    		SetorDAO setorDAO = new SetorDAO();
   	    		setorDAO.atualizar(setor);
   	    		modelo.removeRow(tabelaSetor.getSelectedRow());
   	 			modelo.addRow(setor);
   	 			Mensageiro.mostraMensagemSucesso("alteracao", NMMODULO);
   	 			bLimparAction();
   	 			tabelaSetor.getSelectionModel().clearSelection();
   	    	 }catch(Exception e) {
	 			Mensageiro.mostraMensagemErro("alteração", NMMODULO, e.getMessage());
	 		}

    	}  
     }
     
	 private void preencheCampos() {
			try {
				setorBanco = new SetorDAO();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    	
	    	 SetorTableModel modelo = (SetorTableModel) tabelaSetor.getModel();
	    	 int linhaSelecionada = tabelaSetor.getSelectedRow();  
	    	 Setor setorSelecionado = modelo.getRowAt(linhaSelecionada);
	    	 
	    	 tNmSetor.setText(setorSelecionado.getNmSetor());
	    	 tSiglaSetorSuap.setText(setorSelecionado.getNmSetorSuap());
	}
     
     public void bLimparAction() {
    	 Forms.clearTextFields(this.getContentPane());    	 
     }

	
     public void carregaDados() throws Exception {
    	 
    	 SetorTableModel modelo = new SetorTableModel();
    	 SetorDAO setorDao = new SetorDAO(); 
    	 List<Setor> dados =  (List<Setor>) setorDao.listaTodos();
    	 
    	 
    	 for(Setor s: dados) {
    		 modelo.addRow(s);
    	 }
    	 
    	 tabelaSetor.setModel(modelo);
     }
	
	public static void main(String[] args) throws Exception {
		JanelaSetor janelaSetor = new JanelaSetor();
		janelaSetor.setVisible(true);
	}

}

//Kaíque Matheus
package br.tatuapu.view;

import javax.swing.*;

import java.util.List;
import br.tatuapu.model.Discente;
import br.tatuapu.model.DiscenteDAO;
import br.tatuapu.model.DiscenteTableModel;

import br.tatuapu.util.Forms;
import br.tatuapu.util.Mensageiro;

import static br.tatuapu.util.Contexto.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;

public class JanelaDiscentes extends JFrame {

	private static final String NMMODULO = "Discente";
	private JTextField tIdDiscente;
	private JTextField tDiscenteNome;
	private JTextField tDiscenteCPF;
	private JTable tabelaDiscente;
	DiscenteDAO discenteBanco;
	private JPanel painelSuperior;
	protected boolean flag;
	Discente discente = null;

	public JanelaDiscentes() throws Exception {

		super(APLICACAONOME + " - " + APLICACAOVERSAO + " - Cadastro de Discentes");
		setSize(APLICACAOWIDTH, APLICACAOHEIGHT);

		// Centralizando
		setLocationRelativeTo(null);

		JPanel painel = new JPanel(new BorderLayout());
		JToolBar bar = new JToolBar();
		bar.setFloatable(false);

		// Painel do Formulário
		painelSuperior = new JPanel(new GridLayout(3, 2, 0, 0));

		// criando a estrutura da janela
		JLabel lDiscenteNome = new JLabel("Nome do Discente:");
		painelSuperior.add(lDiscenteNome);

		tDiscenteNome = new JTextField(15);
		painelSuperior.add(tDiscenteNome);

		JLabel lDiscenteCPF = new JLabel("CPF:");
		painelSuperior.add(lDiscenteCPF);

		tDiscenteCPF = new JTextField(15);
		painelSuperior.add(tDiscenteCPF);

		painel.add("North", painelSuperior);
		painel.add(bar);

		JPanel painelCentral = new JPanel(new BorderLayout());
		painelCentral.add("North", bar);

		tabelaDiscente = new JTable();
		DiscenteTableModel modelo = new DiscenteTableModel();
		tabelaDiscente.setModel(modelo);

		JScrollPane barraRolagem = new JScrollPane(tabelaDiscente);
		painelCentral.add("Center", barraRolagem);

		// painelCentral.add("Center", tabelaDiscente);

		painel.add("Center", painelCentral);

		// botões

		JButton bDeletar = new JButton("Deletar");
		bar.add(bDeletar);

		JButton bSalvar = new JButton("Salvar");
		bar.add(bSalvar);

		JButton bLimpar = new JButton("Limpar");
		bar.add(bLimpar);

		setContentPane(painel);

		// Funções dos botões

		bSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				try {
					bSalvarAction();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		bDeletar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				bDeletarAction();
			}
		});

		bLimpar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				bLimparAction();
			}
		});

		tabelaDiscente.addMouseListener(new java.awt.event.MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() > 1) {
					flag = true;
					preencheCampos();
				}

			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

		});

		tDiscenteCPF.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				List discente;
				String[] discenteCheck = { "0", "", tDiscenteCPF.getText() };

				try {
					discenteBanco = new DiscenteDAO();

					discente = discenteBanco.procura(discenteCheck);

					if (!discente.isEmpty())
						preencheDados(discente);
				} catch (Exception e1) {
					e1.printStackTrace();
				}

			}

			@Override
			public void focusGained(FocusEvent e) {

			}
		});

		carregarDados();
	}

	private void carregarDados() throws Exception {
		DiscenteTableModel modelo = new DiscenteTableModel();
		DiscenteDAO dao = new DiscenteDAO();

		List<Discente> dados = (List<Discente>) dao.listaTodos();

		for (Discente d : dados) {
			modelo.addRow(d);
		}
		tabelaDiscente.setModel(modelo);
	}

	protected void bSalvarAction() throws Exception {
		if (tabelaDiscente.getSelectedRow() == -1) {
			try {
				if (!tDiscenteNome.getText().isEmpty() && !tDiscenteCPF.getText().isEmpty()) {
					Discente discente = new Discente(0, 0, tDiscenteNome.getText(), tDiscenteCPF.getText());
					DiscenteDAO discenteDAO = new DiscenteDAO();
					DiscenteTableModel modelo = (DiscenteTableModel) this.tabelaDiscente.getModel();
					discenteDAO.salvar(discente);
					modelo.addRow(new Discente(0, 0, tDiscenteNome.getText(), tDiscenteCPF.getText()));
					bLimparAction();
					Mensageiro.mostraMensagemSucesso("novo", NMMODULO);
				} else {
					Mensageiro.mostraMensagemErro("remoção", NMMODULO, "Campos Vazio");
				}

			} catch (Exception e) {
				Mensageiro.mostraMensagemErro("novo", NMMODULO, e.getMessage());
			}

		}else {
			DiscenteTableModel modelo = (DiscenteTableModel) this.tabelaDiscente.getModel();
	       	 int linhaSelecionada = tabelaDiscente.getSelectedRow(); 
	       	 try {
	       		 Discente discenteSelecionado = modelo.getRowAt(linhaSelecionada);
	       		 Discente discente = new Discente(discenteSelecionado.getIdDiscente(), discenteSelecionado.getIdPessoa(), tDiscenteNome.getText(), tDiscenteCPF.getText());
	       		 DiscenteDAO discenteDAO = new DiscenteDAO();
	       		 discenteDAO.atualizar(discente);
	       		 modelo.removeRow(tabelaDiscente.getSelectedRow());
	       		 modelo.addRow(discente);
	       		 Mensageiro.mostraMensagemSucesso("alteração", NMMODULO);
	       		 bLimparAction();
	       		 tabelaDiscente.getSelectionModel().clearSelection();
	       	 }catch(Exception e) {
	       		Mensageiro.mostraMensagemErro("alteração", NMMODULO, e.getMessage());
	       	 }
		}
		carregarDados();

	}

	private void bDeletarAction() {
		try {
			this.discenteBanco = new DiscenteDAO();
		} catch (Exception e) {
			e.printStackTrace();
		}
		DiscenteTableModel modelo = (DiscenteTableModel) this.tabelaDiscente.getModel();
		int linhaSelecionada = tabelaDiscente.getSelectedRow();

		if (linhaSelecionada >= 0) {
			try {
				Discente discenteSelecionado = modelo.getRowAt(linhaSelecionada);
				discenteBanco.excluir(discenteSelecionado);
				modelo.removeRow(tabelaDiscente.getSelectedRow());
				JOptionPane.showMessageDialog(null, "Deletado com sucesso");
			} catch (Exception e) {
				Mensageiro.mostraMensagemErro("remoção", NMMODULO, e.getMessage());
			}
		} else {
			Mensageiro.mostraMensagemGenerica("Selecione um Discente para excluir!");
		}
	}

	private void bLimparAction() {
		Forms.clearTextFields(painelSuperior);

	}

	private void preencheCampos() {
		try {
			discenteBanco = new DiscenteDAO();
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		DiscenteTableModel modelo = (DiscenteTableModel) tabelaDiscente.getModel();
		int linhaSelecionada = tabelaDiscente.getSelectedRow();
		Discente setorSelecionado = modelo.getRowAt(linhaSelecionada);

		tDiscenteNome.setText(setorSelecionado.getNmPessoa());
		tDiscenteCPF.setText(setorSelecionado.getCpf());

	}

	private void preencheDados(List discente) {
		tDiscenteNome.setText(((Discente) discente.get(0)).getNmPessoa());

	}

	public static void main(String[] args) throws Exception {
		JanelaDiscentes j = new JanelaDiscentes();
		j.setVisible(true);
	}
}
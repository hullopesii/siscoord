/*Feito por Leozao*/

package br.tatuapu.view;

import static br.tatuapu.util.Contexto.APLICACAOHEIGHT;
import static br.tatuapu.util.Contexto.APLICACAONOME;
import static br.tatuapu.util.Contexto.APLICACAOVERSAO;
import static br.tatuapu.util.Contexto.APLICACAOWIDTH;

import java.awt.BorderLayout;
import java.awt.HeadlessException;

import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;
import br.tatuapu.controller.Controlador;
import br.tatuapu.model.LoginDAO;
import br.tatuapu.model.Pessoa;
import br.tatuapu.model.Professor;
import br.tatuapu.model.TAE;
import br.tatuapu.util.ConnectionDAO;
import br.tatuapu.util.Mensageiro;
import br.tatuapu.view.*;

/**
 *
 * @author tatuapu
 */
public class JanelaLogin extends JFrame {

	private final JTextField tUsr;
	private final JPasswordField tPassWd;
	private final JButton btnEntrar;

	public JanelaLogin() {
		super(APLICACAONOME + " - " + APLICACAOVERSAO);
		setSize(APLICACAOWIDTH, APLICACAOHEIGHT);

		JPanel painel = new JPanel();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// centralizando
		setLocationRelativeTo(null);

		painel.add(new JLabel("Digite seu usu�rio:"));
		tUsr = new JTextField(15);
		painel.add(tUsr);

		painel.add(new JLabel("Digite sua senha: "));
		tPassWd = new JPasswordField(15);
		painel.add(tPassWd);

		btnEntrar = new JButton("Entrar");
		painel.add(btnEntrar);

		setContentPane(painel);
		painel.setVisible(true);

		// a��es
		getRootPane().setDefaultButton(btnEntrar);
		btnEntrar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnEntrar();
			}
		});
	}

	protected void btnEntrar() {
		boolean checa = true;

		if (tUsr.getText().equals("")) {

			checa = false;
			tUsr.requestFocus();
			JOptionPane.showMessageDialog(null, "Preencha corretamente o nome!");

		} else if (tPassWd.getPassword().length <= 0) {

			checa = false;
			tPassWd.requestFocus();
			JOptionPane.showMessageDialog(null, "Preencha corretamente a senha!");
		}

		if (checa) {

			String u = tUsr.getText();
			//String p = new String(tPassWd.getPassWd());
			String p = tPassWd.getPassword().toString();

			LoginDAO dao = new LoginDAO();
			try {
				if(dao.verificaUsuario(u, p)) {
					JOptionPane.showMessageDialog(null, "Seja Bem-Vindo");
					this.dispose(); // fechar a janela de login sem fechar o programa
					Controlador controlador = new Controlador();
				}else {
					Mensageiro.mostraMensagemGenerica("Dados Incorretos!");
					System.exit(0);
				}
			} catch (HeadlessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				Mensageiro.mostraMensagemGenerica("Erro\n\n"+e.getMessage());
			}

			
			}
		}

	public static void main(String args[]) throws Exception {
		JanelaLogin login = new JanelaLogin();
	}
}

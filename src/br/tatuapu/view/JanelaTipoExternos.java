package br.tatuapu.view;

import static br.tatuapu.util.Contexto.APLICACAOHEIGHT;
import static br.tatuapu.util.Contexto.APLICACAONOME;
import static br.tatuapu.util.Contexto.APLICACAOVERSAO;
import static br.tatuapu.util.Contexto.APLICACAOWIDTH;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;

import br.tatuapu.model.Discente;
import br.tatuapu.model.DiscenteDAO;
import br.tatuapu.model.DiscenteTableModel;
import br.tatuapu.model.Externo;
import br.tatuapu.model.ExternoDAO;
import br.tatuapu.model.ExternosTabelaModel;
import br.tatuapu.util.Forms;
import br.tatuapu.util.Mensageiro;

//Wellinson Henrique de Souza

public class JanelaTipoExternos extends JFrame {
	private static final long serialVersionUID = 1L;
	private static final String NMMODULO = null;
	JTextField tTipoExternoEndereco;
	JTextField tTipoExternoTelefone;
	JTextField tTipoExternoIDExterno;
	JTextField tTipoExternoIDPessoa;
	JTextField tTipoExternoNome;
	JTextField tTipoExternoCPF;
	ExternoDAO externoDao;
	Externo externo = null;

	JPanel painelSuperior;
	private JTable ExternosTableModel;

	public JanelaTipoExternos() {
		super(APLICACAONOME + " - " + APLICACAOVERSAO + " - Cadastro de Externos");
		setSize(APLICACAOWIDTH, APLICACAOHEIGHT);

		// centralizando

		setLocationRelativeTo(null);

		JPanel painel = new JPanel(new BorderLayout());

		JToolBar bar = new JToolBar();
		bar.setFloatable(false);

		// Formulario

		painelSuperior = new JPanel(new GridLayout(4, 2, 0, 0));
		painel.setLayout(new BorderLayout());
		painel.add("North", painelSuperior);

		// Criando estrutura da Tela

		/*
		 * JLabel lTipoIdPessoa = new JLabel("ID-Pessoa:");
		 * painelSuperior.add(lTipoIdPessoa); tTipoExternoIDPessoa = new
		 * JTextField(15); painelSuperior.add(tTipoExternoIDPessoa);
		 */

		JLabel lTipoNome = new JLabel("Nome:");
		painelSuperior.add(lTipoNome);
		tTipoExternoNome = new JTextField(15);
		painelSuperior.add(tTipoExternoNome);

		JLabel lTipoCPF = new JLabel("CPF:");
		painelSuperior.add(lTipoCPF);
		tTipoExternoCPF = new JTextField(15);
		painelSuperior.add(tTipoExternoCPF);

		/*
		 * JLabel lTipoIdExterno = new JLabel("ID-Externo:");
		 * painelSuperior.add(lTipoIdExterno); tTipoExternoIDExterno = new
		 * JTextField(15); painelSuperior.add(tTipoExternoIDExterno);
		 */

		JLabel lTipoExternoEndereco = new JLabel("Endereco:");
		painelSuperior.add(lTipoExternoEndereco);
		tTipoExternoEndereco = new JTextField(15);
		painelSuperior.add(tTipoExternoEndereco);

		JLabel lTipoTelefone = new JLabel("Telefone:");
		painelSuperior.add(lTipoTelefone);
		tTipoExternoTelefone = new JTextField(15);
		painelSuperior.add(tTipoExternoTelefone);

		JPanel painelCentral = new JPanel(new BorderLayout());
		painelCentral.add("North", bar);
		painel.add("Center", painelCentral);

		// Botoes

		JButton bDeletar = new JButton("Deletar");
		bar.add(bDeletar);

		JButton bSalvar = new JButton("Salvar");
		bar.add(bSalvar);

		JButton bLimpar = new JButton("Limpar");
		bar.add(bLimpar);

		// Tabela

		ExternosTableModel = new JTable();
		JScrollPane barraRolagem = new JScrollPane(ExternosTableModel);

		painelCentral.add("Center", barraRolagem);

		ExternosTabelaModel modelo = new ExternosTabelaModel();
		ExternosTableModel.setModel(modelo);

		setContentPane(painel);

		bDeletar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					bDeletarAction();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});

		bLimpar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				bLimparAction();

			}
		});

		bSalvar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					bSalvarAction();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

		ExternosTableModel.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub

				if (e.getClickCount() > 1)
					preencheCampos();

			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}
		});

		try {
			carregarDados();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}

	private void bDeletarAction() {
		try {
			this.externoDao= new ExternoDAO();
		}catch(Exception e){
			e.printStackTrace();
		}
		ExternosTabelaModel modelo = (ExternosTabelaModel) this.ExternosTableModel.getModel();
		int linhaSelecionada = ExternosTableModel.getSelectedRow();
		
		if(linhaSelecionada >= 0) {
			try {
				Externo externoSelecionado = modelo.getRowAt(linhaSelecionada);
				externoDao.excluir(externoSelecionado);
				modelo.removeRow(ExternosTableModel.getSelectedRow());
				JOptionPane.showMessageDialog(null, "Deletado com sucesso");
			}catch(Exception e) {
				Mensageiro.mostraMensagemErro("remoção", NMMODULO, e.getMessage());
			}
		}else {
			 Mensageiro.mostraMensagemGenerica("Selecione um Externo para excluir!");
		}
	}

	public void bLimparAction() {
		limpaTela();
	}

	private void limpaTela() {
		Forms.clearTextFields(painelSuperior);
	}

	protected void bSalvarAction() throws Exception {
		
		Externo externo = new Externo(0,tTipoExternoNome.getText(),tTipoExternoCPF.getText(),0,tTipoExternoEndereco.getText()
				,tTipoExternoTelefone.getText());
		ExternoDAO externoDAO = new ExternoDAO();
		ExternosTabelaModel modelo = (ExternosTabelaModel) this.ExternosTableModel.getModel();
		externoDAO.salvar(externo);
		modelo.addRow(new Externo(externoDAO.getLastID(),tTipoExternoNome.getText(),tTipoExternoCPF.getText(),0,tTipoExternoEndereco.getText()
				,tTipoExternoTelefone.getText()));
		bLimparAction();
		Mensageiro.mostraMensagemSucesso("novo", NMMODULO);
	}
	

	private void carregarDados() throws Exception {
		List<Externo> lista;
		externoDao = new ExternoDAO();
		lista = externoDao.listaTodos();
		ExternosTabelaModel model = new ExternosTabelaModel();

		for (Externo e : lista) {
			model.addRow(e);
		}

		ExternosTableModel.setModel(model);
	}

	private void preencheCampos() {

		ExternosTabelaModel model = (ExternosTabelaModel) ExternosTableModel.getModel();
		int id = ExternosTableModel.getSelectedRow();

		externo = model.getRowAt(id);

		tTipoExternoNome.setText(externo.getNmPessoa());
		tTipoExternoCPF.setText(externo.getCpf());
		tTipoExternoEndereco.setText(externo.getEndereco());
		tTipoExternoTelefone.setText(externo.getTelefone());

	}

	public static void main(String args[]) {
		JanelaTipoExternos j = new JanelaTipoExternos();
		j.setVisible(true);
	}
}
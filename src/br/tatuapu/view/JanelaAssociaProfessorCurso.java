package br.tatuapu.view;

import static br.tatuapu.util.Contexto.APLICACAOHEIGHT;
import static br.tatuapu.util.Contexto.APLICACAONOME;
import static br.tatuapu.util.Contexto.APLICACAOVERSAO;
import static br.tatuapu.util.Contexto.APLICACAOWIDTH;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;

import br.tatuapu.model.CoordenadorComboBoxModel;
import br.tatuapu.model.CursoTableModel;
import br.tatuapu.model.Professor;
import br.tatuapu.model.ProfessorComboBoxModel;
import br.tatuapu.model.ProfessorDAO;

public class JanelaAssociaProfessorCurso extends JFrame {

	JPanel painelSuperior;
	JTextField tCurso;
	JTable tabelaSetor;
	JComboBox cbbProfessor;
	
	public JanelaAssociaProfessorCurso() throws Exception {
		super(APLICACAONOME + " - " + APLICACAOVERSAO + " - Associação Professor e Curso");
		setSize(APLICACAOWIDTH,APLICACAOHEIGHT);
		//centralizando
		setLocationRelativeTo(null);
		
		JPanel painel = new JPanel(new BorderLayout());
		JToolBar bar = new JToolBar();
		bar.setFloatable(false);
		
		//painel do formulário
		painelSuperior = new JPanel(new GridLayout(2,2,0,0));
		
		JLabel lCurso = new JLabel("Curso: ");
		painelSuperior.add(lCurso);
		
		tCurso = new JTextField(15);
		tCurso.setEditable(false);
		painelSuperior.add(tCurso);	
		
		JLabel lProfessor = new JLabel("Professor: ");
		painelSuperior.add(lProfessor);
		
		cbbProfessor = new JComboBox();
//		ProfessorComboBoxModel cbbProfessorModel = new ProfessorComboBoxModel();
//		cbbProfessor.setModel(cbbProfessorModel);
		preencherComboBox();
		
		painelSuperior.add(cbbProfessor);
		
		painel.add("North", painelSuperior);
		
		JPanel painelCentral = new JPanel(new BorderLayout());
		painelCentral.add("North",bar);
		
		tabelaSetor = new JTable();
		CursoTableModel modelo = new CursoTableModel();
		tabelaSetor.setModel(modelo);
		
		JScrollPane barraRolagem = new JScrollPane(tabelaSetor);
		painelCentral.add("Center", barraRolagem);
		
		painel.add("Center",painelCentral);
		
		JButton bAssociar = new JButton("Associar");
		bar.add(bAssociar);
				
		setContentPane(painel);
		
	}
	
	void preencherComboBox() {
		try {
			List<Professor> lista;
			ProfessorDAO profBanco = new ProfessorDAO();
			lista = profBanco.listaTodos();
			ProfessorComboBoxModel modelo = new ProfessorComboBoxModel();
			for(Professor p: lista) {
				modelo.addItem(p);
			}
			cbbProfessor.setModel(modelo);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "problema ao carregar dados2!"+e.getMessage());
		}
	}
	
	public static void main (String args[]) throws Exception {
		JanelaAssociaProfessorCurso janelaAssocia = new JanelaAssociaProfessorCurso();
		janelaAssocia.setVisible(true);
	}
	
}

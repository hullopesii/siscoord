package br.tatuapu.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.tatuapu.util.ConnectionDAO;
import br.tatuapu.util.DAO;

public class CoordenadorDAO implements DAO{

	private Connection conn;

	@Override
	public void atualizar(Object ob) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void excluir(Object ob) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public List listaTodos() throws Exception {
		PreparedStatement ps = null;
		Connection conn = null;
		ResultSet rs = null;
		ArrayList<Professor> coordenadores = new ArrayList<>();

		try{
			String SQL ="select * from siscoord.Professor_has_Portaria "
					+ "inner join siscoord.Professor "
					+ "on Professor.idProfessor = Professor_has_Portaria.idProfessorHasPortaria "
					+ "inner join siscoord.Pessoa "
					+ "on Pessoa.idPessoa = Professor.idPessoa;";
			conn = ConnectionDAO.getConnection();
			ps = conn.prepareStatement(SQL);
			rs = ps.executeQuery();
			while(rs.next()) {
				Professor prof = new Professor(rs.getInt(10),rs.getString(11),rs.getString(12),rs.getString(7),rs.getString(6),rs.getString(8),rs.getString(9),rs.getInt(4));
				coordenadores.add(prof);
			}
		}catch (SQLException sqle){
			throw new Exception("Erro ao buscar dados: \n"+sqle);
		}finally{
			ConnectionDAO.closeConnection(conn,ps);
		}
		return coordenadores;
	}
	@Override
	public List procura(Object ob) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void salvar(Object ob) throws Exception {
		// TODO Auto-generated method stub

	}

}

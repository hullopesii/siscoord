/*Criado por Leozao*/

package br.tatuapu.model;

public class TAE extends Pessoa {
	private final int idTae;
	private final String user;
	private final String passWd;
	private final Setor setor;

	public TAE(int idPessoa, int id, String nm, String cpf, String user, String passWd, Setor setor) {
		super(idPessoa, nm, cpf);
		this.idTae = id;
		this.user = user;
		this.passWd = passWd;
		this.setor = setor;
	}

	public Setor getSetor() {
		return this.setor;
	}
	
	public int getIdTAE() {
		
		return this.idTae;
	}

	@Override
	public String getMatricula() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getUser() {
		return user;
	}

	public String getPassWd() {
		return passWd;
	}

}

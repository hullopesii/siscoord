package br.tatuapu.model;
/*
 * @author Rúben França Xavier
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Statement;

import br.tatuapu.util.ConnectionDAO;
import br.tatuapu.util.DAO;

public class PessoaDAO implements DAO {

	private Connection conn;
	private int lastID;

	public PessoaDAO() throws Exception {
		try{
			this.conn = ConnectionDAO.getConnection();
		}catch(Exception e){
			throw new Exception("Erro: \n"+e.getMessage());                    
		}
	}

	@Override
	public void atualizar(Object ob) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void excluir(Object ob) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public List listaTodos() throws Exception {
		PreparedStatement ps = null;
		Connection conn = null;
		ResultSet rs = null;
		ArrayList<Pessoa> pessoas = new ArrayList<>();
		try{
			String SQL = "Select * from pessoa";
			conn = this.conn;
			ps = conn.prepareStatement(SQL);
			rs = ps.executeQuery();
			while(rs.next()) {
				//pessoas.add();
				rs.getInt(1);
			}
		}catch (SQLException sqle){
			throw new Exception("Erro ao inserir dados do autor: \n"+sqle);
		}finally{
			ConnectionDAO.closeConnection(conn,ps);
		}
		return null;
	}

	/**
	 * Metodo para procurar pessoas
	 * @Object ob sendo um Array de String com 3 posições 
	 * posição 0 idPessoa
	 * posição 1 nomePessoa
	 * posicção 2 cpf
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public List procura(Object ob) throws Exception {
		String[] pessoa = (String[]) ob;
		
		PreparedStatement ps = null;
		Connection conn = null;
		ResultSet rs = null;
		ArrayList<Pessoa> pessoas = new ArrayList<>();
		try{
			String SQL = "Select * from Pessoa where idPessoa = ? OR nmPessoa = ? OR cpf = ?";
			
			conn = ConnectionDAO.getConnection();
			ps = conn.prepareStatement(SQL);
			ps.setInt(1,Integer.parseInt(pessoa[0]));
			ps.setString(2, pessoa[1]);
			ps.setString(3, pessoa[2]);
			rs = ps.executeQuery();

			while(rs.next()) {
				//pessoas.add();
				int idPessoa = rs.getInt(1);
				String nm = rs.getString(2);
				String cpf = rs.getString(3);
				Discente d = new Discente(idPessoa, 0, nm, cpf, null);
				pessoas.add(d);
			}
		}catch (SQLException sqle){
			throw new Exception("Erro ao inserir dados do autor: \n"+sqle);
		}finally{
			ConnectionDAO.closeConnection(conn,ps);
		}
		return pessoas;
	}

	@Override
	public void salvar(Object ob) throws Exception {
		String[] pessoa = (String[]) ob;
		String nome = pessoa[0];
		String cpf = pessoa[1];
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		try{
			conn = ConnectionDAO.getConnection();

			String SQL_Select = "SELECT * FROM siscoord.Pessoa WHERE nmPessoa = ? AND cpf = ?;";
			ps=conn.prepareStatement(SQL_Select);
			ps.setString(1, nome);
			ps.setString(2, cpf);
			rs = ps.executeQuery();

			while(rs.next()) {
				lastID = rs.getInt(1);
			}
			if (lastID <=0) {

				String SQL = "INSERT INTO Pessoa VALUES (null,?,?);";
				ps = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, nome);
				ps.setString(2, cpf);

				//tratando da inserção e coletando último id;

				int affectedRows = ps.executeUpdate();

				if (affectedRows == 0) 
					throw new SQLException();

				rs = ps.getGeneratedKeys();
				if (rs.next()) {
					this.lastID = rs.getInt(1);
				}else{
					this.lastID = 0;
				}
			}

		}catch (SQLException sqle){
			throw new Exception("Erro ao salvar um novo Professor: \n"+sqle);
		}finally{
			ConnectionDAO.closeConnection(conn,ps);
		}
	}
	public int getLastID(){
		return this.lastID;
	}



}

package br.tatuapu.model;

public abstract class Pessoa {
	private final int idPessoa;
	private final String nmPesoa;
	private final String cpf;
	
	public Pessoa(int id, String nm, String cpf) {
		this.idPessoa = id;
		this.nmPesoa = nm;
		this.cpf = cpf;
	}

	public String getNmPessoa() {
		return nmPesoa;
	}
	
	public abstract String getMatricula();

	public String getCpf() {
		return cpf;
	}

	public int getIdPessoa() {
		return idPessoa;
	}
	
	@Override
	public String toString() {
		return nmPesoa;
	}
	
}
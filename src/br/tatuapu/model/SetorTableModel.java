package br.tatuapu.model;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

public class SetorTableModel extends AbstractTableModel {

	private ArrayList<Setor> dados;
	private String colunas[] = {"ID Setor", "Nome do Setor", "Sigla do Setor"};
	
	public SetorTableModel() {
		dados = new ArrayList<Setor>();
	}
	
	public void addRow(Setor s) {
		this.dados.add(s);
		this.fireTableDataChanged();
	}
	
	public void removeRow(int linha) {
		this.dados.remove(linha);
		this.fireTableRowsDeleted(linha, linha);
	}
	
	@Override
	public int getColumnCount() {
		return this.colunas.length;
	}

	@Override
	public int getRowCount() {
		return this.dados.size();
	}
	
	public String getColumnName(int col) {
		return this.colunas[col];
	}

	@Override
	public Object getValueAt(int linha, int coluna) {
		switch(coluna) {
			case 0: return this.dados.get(linha).getIdSetor();
			case 1: return this.dados.get(linha).getNmSetor();
			case 2: return this.dados.get(linha).getNmSetorSuap();
		}
		return null;
	}

	public ArrayList<Setor> getSetor() {
		return this.dados;
	}
	
	public Setor getRowAt(int linha){
		return this.dados.get(linha);
	}
	
}
package br.tatuapu.model;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import br.tatuapu.model.Portaria;

public class TipoDocumentoTableModel extends AbstractTableModel {

	private ArrayList<TipoDocumento> dados;
	private String colunas[] = {"ID doc", "Nome", "Descrição"};
	
	public TipoDocumentoTableModel() {
		
		this.dados = new ArrayList<TipoDocumento>();
	}
	
	public void addRow(TipoDocumento p) {
		
		this.dados.add(p);
		this.fireTableDataChanged();
	}
	
	public void removeRow(int linha) {
		this.dados.remove(linha);
		this.fireTableRowsDeleted(linha, linha);
	}
	
	public int getColumnCount() {
		
		return this.colunas.length;
	}

	@Override
	public int getRowCount() {
		return this.dados.size();
	}
	
	public String getColumnName(int col) {
		return this.colunas[col];
	}

	@Override
	public Object getValueAt(int linha, int coluna) {
		
		switch(coluna) {
		
		case 0: return this.dados.get(linha).getIdTipoDocumento();
		case 1: return this.dados.get(linha).getTipoDocumentoNome();
		case 2: return this.dados.get(linha).getTipoDocumentoDesc();
		
		}
		return null;
	}
	
      public ArrayList<TipoDocumento> getPortaria() {
		
		return this.dados;
	}
      public TipoDocumento getRowAt(int linha){
          return this.dados.get(linha);
      }

	public void atualizaLinha(int linha, TipoDocumento tipoDocumento) {
		this.removeRow(linha);
		this.addRow(tipoDocumento);
	}

}

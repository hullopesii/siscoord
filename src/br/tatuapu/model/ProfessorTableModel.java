package br.tatuapu.model;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

/*
 * @author Rúben França 
 */

@SuppressWarnings("serial")
public class ProfessorTableModel extends AbstractTableModel {

	private ArrayList<Professor> dados;
	private String[] colunas = {"ID","Nome","CPF","Area","Matrícula"}; 

	public ProfessorTableModel() {
		dados = new ArrayList<Professor>();
	}

	public void addRow(Professor d) {
		dados.add(d);
		fireTableDataChanged();
	}

	public void removeRow(int linha) {
		this.dados.remove(linha);
		this.fireTableRowsDeleted(linha, linha);
	}


	@Override
	public int getColumnCount() {
		return this.colunas.length;
	}

	@Override
	public int getRowCount() {
		return this.dados.size();
	}

	public String getColumnName(int col) {
		return this.colunas[col];
	}


	@Override
	public Object getValueAt(int linha, int coluna) {
		switch (coluna) {
		case 0 : return this.dados.get(linha).getIdProfessor();
		case 1 : return this.dados.get(linha).getNmPessoa();
		case 2 : return this.dados.get(linha).getCpf();
		case 3 : return this.dados.get(linha).getArea();
		case 4 : return this.dados.get(linha).getMatricula();
		}
		return null;
	}

	public ArrayList<Professor> getProfessor(){
		return this.dados;
	}
	
	public Professor getRowAt(int linha) {
		return this.dados.get(linha);
	}

}

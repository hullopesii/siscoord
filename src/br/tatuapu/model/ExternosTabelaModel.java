package br.tatuapu.model;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

//Wellinson Henrique de Souza

public class ExternosTabelaModel extends AbstractTableModel {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<Externo> dados;
	private String[] colunas = {"ID-Pessoa","Nome","CPF","ID-Externo","Endereço","Telefone"};
	
	public ExternosTabelaModel() 
	{
		dados = new ArrayList<Externo>();
	}

	public ArrayList<Externo> getProduto()
	{
		return dados;
	}
	public String getColumnName(int col)
	{
		return this.colunas[col];
	}

	public void addRow(Externo p)
	{
		this.dados.add(p);
		this.fireTableDataChanged();
	}
	
	public void removeRow(int linha)
	{
		this.dados.remove(linha);
		this.fireTableRowsDeleted(linha,linha);
	}
	
	@Override
	public int getColumnCount() 
	{
		return this.colunas.length;
	}

	@Override
	public int getRowCount() 
	{
		return this.dados.size();
	}

	@Override
	public Object getValueAt(int linha, int coluna)
	{
		switch(coluna)
		{
		case 0: return this.dados.get(linha).getIdPessoa();
		case 1: return this.dados.get(linha).getNmPessoa();
		case 2: return this.dados.get(linha).getCpf();
		case 3: return this.dados.get(linha).getIdExterno();
		case 4: return this.dados.get(linha).getEndereco();
		case 5: return this.dados.get(linha).getTelefone();
		}
		return null;
	}

	public Externo getRowAt(int linha) {
		return this.dados.get(linha);
	}

}


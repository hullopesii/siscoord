/*Feito por Leozao*/

package br.tatuapu.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.tatuapu.util.ConnectionDAO;

public class LoginDAO {

	private Connection conn;

	public Boolean verificaUsuario(Object usr, Object passwd) throws Exception {

		PreparedStatement ps = null;
		//PreparedStatement ps2 = null;
		Connection conn = null;
		ResultSet rs = null;
		//ResultSet rs2 = null;
		String acesso;
		boolean autenticado = false;
		List<Professor> professores = new ArrayList<Professor>();
		List<TAE> taes = new ArrayList<TAE>();
		try {

			// verificação para caso o usuario seja professor
			String SQL = "Select * from professor INNER JOIN pessoa ON pessoa.idPessoa = professor.idPessoa WHERE professor.user =? AND professor.passwd =?";

			conn = this.conn;
			ps = conn.prepareStatement(SQL);
			ps.setString(1, (String) usr);
			ps.setString(2, (String) passwd);
			rs = ps.executeQuery();
			
			while (rs.next()) { // caso o usuario esteja em professor
				//pegar os dados de professor
				Professor p = new Professor(rs.getInt(1),rs.getInt(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getInt(7),rs.getString(8),rs.getString(9));
				
				professores.add(p);
						
				/*String user = rs.getString("username");
				String pass = rs.getString("senha");
				acesso = rs.getString("acesso");// Aqui armazeno o acesso*/
				autenticado = true;

				ps.close();
			} 
			
			if(professores.size()==0) {
				
				String SQL2 = "Select * from TAE INNER JOIN pessoa ON pessoa.idPessoa = tae.idPessoa WHERE TAE.user =? AND TAE.passwd =?";
				
				//pegar os dados de TAE
				TAE t = new TAE(rs.getInt(1),rs.getInt(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getInt(6),rs.getString(7),rs.getString(8));
				
				taes.add(t);
				
				// verificação para caso seja TAE
				
				
				ps = conn.prepareStatement(SQL2);
				ps.setString(1, (String) usr);
				ps.setString(1, (String) passwd);
				rs = ps.executeQuery();
				ps.close();
	
				
				while (rs.next()) { // caso o usuario esteja em TAE
	
					String user = rs.getString("username");
					String pass = rs.getString("senha");
					acesso = rs.getString("acesso");// Aqui armazeno o acesso
					autenticado = true;
	
					ps.close();
	
				}
			}
			
			if(professores.size()==0 && taes.size()==0)
				return false;
			else
				return true;

		} catch (SQLException sqle) {

			throw new Exception("Erro ao inserir dados do usuario: \n" + sqle);

		} finally {

			ConnectionDAO.closeConnection(conn, ps);
		}

		return autenticado; // retornar o autenticado
	}
}

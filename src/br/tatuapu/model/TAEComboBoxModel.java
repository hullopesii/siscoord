package br.tatuapu.model;

import java.util.ArrayList;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

public class TAEComboBoxModel extends AbstractListModel implements ComboBoxModel {

	private ArrayList<TAE> dados;
	private TAE selection;
	
    public TAEComboBoxModel() {
    	
    	this.dados = new ArrayList<TAE>();
    }
    
    public void addItem(TAE p) {
		
		this.dados.add(p);
	}
    
    @Override
	public int getSize() {
		return this.dados.size();				
	}
	
    public ArrayList<TAE> getTAE() {
    	
		return this.dados;
	}

	@Override
	public Object getElementAt(int index) {
		return this.dados.get(index);
	}
	

	public TAE getSelectedItem() {
		return selection;
	}

	public void setSelectedItem(Object p) {
		selection = (TAE) p;		
	}

	


}

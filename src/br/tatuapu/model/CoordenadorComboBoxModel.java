package br.tatuapu.model;
/**
 * @author Victor Hugo
 */

import java.util.ArrayList;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

public class CoordenadorComboBoxModel extends AbstractListModel implements ComboBoxModel {


	private ArrayList<Pessoa> dados;
	private Pessoa selection;
	
	
	public CoordenadorComboBoxModel() {
		
		this.dados = new ArrayList<Pessoa>();
	}
	
	public void addItem(Pessoa c) {
		this.dados.add(c);
	}
	
	@Override
	public int getSize() {
		return this.dados.size();				
	}
	
    public ArrayList<Pessoa> getCurso() {
    	
		return this.dados;
	}

	@Override
	public Object getElementAt(int index) {
		return this.dados.get(index);
	}
	

	@Override
	public Object getSelectedItem() {
		return selection;
	}

	@Override
	public void setSelectedItem(Object anItem) {
		selection = (Pessoa) anItem;		
	}

}

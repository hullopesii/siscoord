//Kaíque Matheus
package br.tatuapu.model;

import java.util.ArrayList;

public class Discente extends Pessoa{
	private final int idDiscente;
	//public final String matricula;
	private ArrayList<Curso> cursos;
	

	public Discente(int idDiscente, int idPessoa, String nm, String cpf) {
		super(idPessoa, nm, cpf);
		this.idDiscente = idDiscente;
		//this.matricula = matricula;
		
	}
	

	public int getIdDiscente() {
		// TODO Auto-generated method stub
		return this.idDiscente;
	}
	
	@Override
	public String getMatricula() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public ArrayList<Curso> getCursos() {
		return cursos;
	}
	
	public void addCurso(Curso curso) {
		cursos.add(curso);
	}
}
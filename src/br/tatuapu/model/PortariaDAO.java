package br.tatuapu.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import com.mysql.jdbc.Statement;

import java.sql.Connection;
import br.tatuapu.util.ConnectionDAO;
import br.tatuapu.util.DAO;

public class PortariaDAO implements DAO {

	private Connection conn;
	private int lastID;

	public PortariaDAO() throws Exception {

		try {

			this.conn = ConnectionDAO.getConnection();
		}

		catch (Exception e) {

			throw new Exception("Erro: \n" + e.getMessage());
		}
	}

	@Override
	public void atualizar(Object ob) throws Exception {

		Portaria p = (Portaria) ob;

		if (p != null) {

			Connection conn = null;
			PreparedStatement ps = null;
			ResultSet rs = null;

			try {

				conn = ConnectionDAO.getConnection();

				String sql = "update siscoord.portaria set nroPortaria = ?, src = ?, tipo = ? where idPortaria = ?";

				ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

				ps.setInt(4, p.getIdPortaria());
				ps.setString(1, p.getNroPortaria());
				ps.setString(2, p.getSrc());
				ps.setString(3, p.getTipo());
				int affectedRows = ps.executeUpdate();

				if (affectedRows == 0)
					throw new SQLException();

			}

			catch (SQLException e) {
				JOptionPane.showMessageDialog(null, "Erro ao Atualizar Dados da Portaria" + e.getMessage());

			}

			finally {

				ConnectionDAO.closeConnection(conn, ps);
				
			}

		}

		else {

			JOptionPane.showMessageDialog(null, "Objeto Nulo!!");
		}
	}

	@Override
	public void excluir(Object ob) throws Exception {

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {

			conn = ConnectionDAO.getConnection();

			String sql = "delete from siscoord.portaria where idPortaria = ?";

			ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			ps.setInt(1, (int) ob);

			int affectedRows = ps.executeUpdate();

			if (affectedRows == 0)
				throw new SQLException();

			rs = ps.getGeneratedKeys();

			if (rs.next()) {
				this.lastID = rs.getInt(1);
			} else {
				this.lastID = 0;
			}

		}

		catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao Excluir Dados da Portaria" + e.getMessage());
			throw new Exception();
			

		}

		finally {

			// ConnectionDAO c = new ConnectionDAO();
			ConnectionDAO.closeConnection(conn, ps);
			// System.out.println("OK!");
		}

	}

	@Override
	public List listaTodos() throws Exception {

		PreparedStatement ps = null;
		Connection conn = null;
		ResultSet rs = null;
		ArrayList<Portaria> portaria = new ArrayList<Portaria>();

		try {

			String SQL = "Select *from portaria";
			conn = this.conn;
			ps = conn.prepareStatement(SQL);
			rs = ps.executeQuery();

			while (rs.next()) {

				Portaria port = new Portaria(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4));
				portaria.add(port);
			}
		}

		catch (SQLException sqle) {
			throw new Exception("Erro ao inserir dados do autor: \n" + sqle);
		}

		finally {
			ConnectionDAO.closeConnection(conn, ps);
		}

		return portaria;
	}

	@Override
	public List procura(Object ob) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void salvar(Object ob) throws Exception {

		Portaria p = (Portaria) ob;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {

			conn = ConnectionDAO.getConnection();

			String sql = "INSERT INTO `Portaria` ( `idPortaria`, `nroPortaria`, `src`, `tipo`)" + "values( ?, ?, ?, ?)";

			ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			ps.setInt(1, p.getIdPortaria());
			ps.setString(2, p.getNroPortaria());
			ps.setString(3, p.getSrc());
			ps.setString(4, p.getTipo());
			int affectedRows = ps.executeUpdate();

			if (affectedRows == 0)
				throw new SQLException();

			rs = ps.getGeneratedKeys();

			if (rs.next()) {
				this.lastID = rs.getInt(1);
			} else {
				this.lastID = 0;
			}
		}

		catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao Salvar Dados da Portaria" + e.getMessage());

		}

		finally {

			// ConnectionDAO c = new ConnectionDAO();
			ConnectionDAO.closeConnection(conn, ps);
			// System.out.println("OK!");
		}

	}

	public int getLastID() {
		return this.lastID;
	}

}

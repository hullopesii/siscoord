package br.tatuapu.model;

public interface Coordenador 
{
	
	public Portaria getPortaria();
	
	public void setPortaria(Portaria p);

}

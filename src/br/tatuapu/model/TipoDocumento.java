package br.tatuapu.model;

public class TipoDocumento {
	private final int idTipoDocumento;
	private final String tipoDocumentoNome;
	private final String tipoDocumentoDesc;
	
	public TipoDocumento(int idTipoDocumento, String tipoDocumentoNome, String tipoDocumentoDesc){
		this.idTipoDocumento = idTipoDocumento;
		this.tipoDocumentoNome = tipoDocumentoNome;
		this.tipoDocumentoDesc = tipoDocumentoDesc;
	}
	
	public int getIdTipoDocumento(){
		return this.idTipoDocumento;
	}
	public String getTipoDocumentoNome(){
		return this.tipoDocumentoNome;
	}
	public String getTipoDocumentoDesc(){
		return this.tipoDocumentoDesc;
	}
	@Override
	public String toString(){
		return tipoDocumentoNome;
	}
}

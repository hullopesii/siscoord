//Kaíque Matheus
package br.tatuapu.model;

import java.util.ArrayList;
import javax.swing.table.*;

public class DiscenteTableModel extends AbstractTableModel {

	private ArrayList<Discente> dados;
	private String colunas[] = { "ID Discente", "Nome", "CPF" };

	public DiscenteTableModel() {
		this.dados = new ArrayList<Discente>();
	}

	public void addRow(Discente d) {
		this.dados.add(d);
		this.fireTableDataChanged(); 
	}

	public void removeRow(int linha) {
		this.dados.remove(linha);
		this.fireTableRowsDeleted(linha, linha);
	}

	public int getColumnCount() {
		return this.colunas.length;
	}

	@Override
	public int getRowCount() {
		return this.dados.size();
	}

	public String getColumnName(int col) {
		return this.colunas[col];
	}

	@Override
	public Object getValueAt(int linha, int coluna) {
		switch (coluna) {
		
		case 0:return this.dados.get(linha).getIdDiscente();
		//case 1:return this.dados.get(linha).getIdPessoa();
		case 1:return this.dados.get(linha).getNmPessoa();
		case 2:return this.dados.get(linha).getCpf();
		//case 3:	return this.dados.get(linha).getMatricula();
		}

		return null;
	}

	public ArrayList<Discente> getDiscente() {

		return this.dados;

	}
	public Discente getRowAt(int linha){
		return this.dados.get(linha);
	}

}
package br.tatuapu.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import com.mysql.jdbc.Statement;

import br.tatuapu.util.ConnectionDAO;

public class AssociaPortariaTAEDAO {

	private Connection conn;
	private int lastID;
	
	public AssociaPortariaTAEDAO() throws Exception {
		
		try {

			this.conn = ConnectionDAO.getConnection();
		}

		catch (Exception e) {

			throw new Exception("Erro: \n" + e.getMessage());
		}
	}
	
	public void associaPortariaProfessor(int idPortaria, int idTAE) throws Exception {

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {

			conn = ConnectionDAO.getConnection();

			String sql = "INSERT INTO tae_has_portaria(`idTAEHasPortaria`,`TAE_idTAE`,`Portaria_idPortaria`) VALUES(?, ?, ?);";

			ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			ps.setInt(1, 0);
			ps.setInt(2, idTAE);
			ps.setInt(3, idPortaria);
			
			int affectedRows = ps.executeUpdate();

			if (affectedRows == 0)
				throw new SQLException();

			rs = ps.getGeneratedKeys();

			if (rs.next()) {
				this.lastID = rs.getInt(1);
			} else {
				this.lastID = 0;
			}
		}

		catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao Relacionar Portaria a TAE" + e.getMessage());

		}

		finally {

			// ConnectionDAO c = new ConnectionDAO();
			ConnectionDAO.closeConnection(conn, ps);
			// System.out.println("OK!");
		}
	}
}

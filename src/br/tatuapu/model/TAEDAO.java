package br.tatuapu.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.tatuapu.util.ConnectionDAO;
import br.tatuapu.util.DAO;

public class TAEDAO implements DAO {

	private Connection conn;
	
	public TAEDAO() throws Exception {
		try {
			this.conn = ConnectionDAO.getConnection();
        }catch(Exception e){
            throw new Exception("Erro: \n"+e.getMessage());                    
        }
	}
	
	@Override
	public void atualizar(Object ob) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void excluir(Object ob) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<TAE> listaTodos() throws Exception {
		PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        ArrayList<TAE> TAEs = new ArrayList<TAE>();
        try{
            String SQL = "Select * from TAE inner join Pessoa on Pessoa.idPessoa = TAE.idPessoa";
            conn = this.conn;
            ps = conn.prepareStatement(SQL);
            rs = ps.executeQuery();
            while(rs.next()) {
            	//int idPessoa, int id, String nm, String cpf, String user, String passWd, Setor setor
            TAEs.add(new TAE(rs.getInt(2),rs.getInt(1),rs.getString(7), rs.getString(8),rs.getString(4),rs.getString(5),null));
            }
        }catch (SQLException sqle){
            throw new Exception("Erro ao inserir dados do TAE: \n"+sqle);
        }finally{
            ConnectionDAO.closeConnection(conn,ps);
        }
		return TAEs;
	}

	@Override
	public List procura(Object ob) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void salvar(Object ob) throws Exception {
		// TODO Auto-generated method stub
		
	}

}

package br.tatuapu.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Statement;

import br.tatuapu.util.ConnectionDAO;
import br.tatuapu.util.DAO;

public class TipoDocumentoDAO implements DAO {

	private Connection conn;
	private int lastID;

	@Override
	public void atualizar(Object ob) throws Exception {
		TipoDocumento tpDoc = (TipoDocumento) ob;
		PreparedStatement ps = null;
		ResultSet rs = null;
        Connection conn = null;
        try{
        	conn = ConnectionDAO.getConnection();
        	String SQL = "Update TipoDocumento set tipoDocumentoNome = ?, "
        			+ "TipoDocumentoDesc = ? Where idTipoDocumento = ?";
        	ps = conn.prepareStatement(SQL);
        	ps.setString(1, tpDoc.getTipoDocumentoNome());
        	ps.setString(2, tpDoc.getTipoDocumentoDesc());
        	ps.setInt(3, tpDoc.getIdTipoDocumento());
        	
        	//tratando da inserção e coletando último id;
        	
        	int affectedRows = ps.executeUpdate();
        	
        	if (affectedRows == 0) 
                throw new SQLException();
        	
        }catch (SQLException sqle){
            throw new Exception("Erro ao salvar dados dos tipos de documentos: \n"+sqle);
        }finally{
            ConnectionDAO.closeConnection(conn,ps);
        }
		
	}

	@Override
	public void excluir(Object ob) throws Exception {
		PreparedStatement ps = null;
        Connection conn = null;
        
        TipoDocumento tpDocumento = (TipoDocumento) ob;
        if(tpDocumento != null){
        	try{
	        	String SQL = "Delete from TipoDocumento Where idTipoDocumento = ?";
	            conn = ConnectionDAO.getConnection();
	            ps = conn.prepareStatement(SQL);
	            ps.setInt(1, tpDocumento.getIdTipoDocumento());
	            int resp = ps.executeUpdate();
	            if(resp == 0 )
	            	throw new Exception("Nenhum registro foi excluído!\n Tente novamente!");
        	}catch(SQLException sqle){
	        	throw new Exception("Erro ao excluir registro! \n "+sqle.getMessage());
	        }
        }else{
        	throw new Exception("Erro ao excluir registro! \n Registro passado é nulo!");
        }
		
	}

	@Override
	public List listaTodos() throws Exception {
		PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        ArrayList<TipoDocumento> tiposDocumento = new ArrayList<>();
       
        try{
            String SQL = "Select * from TipoDocumento order by tipoDocumentoNome";
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement(SQL);
            rs = ps.executeQuery();
            while(rs.next()) {
            	tiposDocumento.add(new TipoDocumento(rs.getInt(1), rs.getString(2), rs.getString(3)));
            }            
        }catch (SQLException sqle){
            throw new Exception("Erro ao buscar dados dos tipos de documentos: \n"+sqle);
        }finally{
            ConnectionDAO.closeConnection(conn,ps);
        }
        return tiposDocumento;
	}

	@Override
	public List procura(Object ob) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void salvar(Object ob) throws Exception {
		TipoDocumento tpDoc = (TipoDocumento) ob;
		PreparedStatement ps = null;
		ResultSet rs = null;
        Connection conn = null;
        try{
        	conn = ConnectionDAO.getConnection();
        	String SQL = "Insert into TipoDocumento values (NULL, ?,?)";
        	ps = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
        	ps.setString(1, tpDoc.getTipoDocumentoNome());
        	ps.setString(2, tpDoc.getTipoDocumentoDesc());
        	
        	//tratando da inserção e coletando último id;
        	
        	int affectedRows = ps.executeUpdate();
        	
        	if (affectedRows == 0) 
                throw new SQLException();
        	
        	rs = ps.getGeneratedKeys();
        	if (rs.next()) {
                this.lastID = rs.getInt(1);
            }else{
            	this.lastID = 0;
            }
            
        	
        }catch (SQLException sqle){
            throw new Exception("Erro ao salvar dados dos tipos de documentos: \n"+sqle);
        }finally{
            ConnectionDAO.closeConnection(conn,ps);
        }
	}
	public int getLastID(){
		return this.lastID;
	}

}

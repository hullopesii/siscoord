package br.tatuapu.model;

public class Externo extends Pessoa {
	private final String endereco;
	private final String telefone;
	private final int IdExterno;
	
	public Externo(int id, String nm, String cpf,int IdExterno, String endereco, String telefone) {
		super(id,nm,cpf);
		this.IdExterno = IdExterno;
		this.endereco = endereco;
		this.telefone = telefone;
	}
	
	public String getTelefone() 
	{
		return this.telefone;
	}

	public String getEndereco() 
	{
		return this.endereco;
	}

	public int getIdExterno() 
	{
		return this.IdExterno;
	}

	@Override
	public String getMatricula() {
		// TODO Auto-generated method stub
		return null;
	}

}

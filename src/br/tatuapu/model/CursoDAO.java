package br.tatuapu.model;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Statement;

import br.tatuapu.util.ConnectionDAO;
import br.tatuapu.util.DAO;

public class CursoDAO implements DAO {

	private Connection conn;
	private int lastID;

	public CursoDAO () throws Exception {
		try {
			this.conn = ConnectionDAO.getConnection();
		}catch(Exception e) {
			throw new Exception("Erro: " + e.getMessage());
		}
	}
	@Override
	public void atualizar(Object ob) throws Exception {
		Curso curso = (Curso) ob;

		if(curso != null) {
			PreparedStatement ps = null;
			Connection conn = null;
			ResultSet rs = null;
			try{
				conn = ConnectionDAO.getConnection();
				String SQL = "UPDATE `siscoord`.`Curso` SET `nmCurso`= ?, `siglaCursoSuap`=? WHERE `idCurso`=?;";
				ps = conn.prepareStatement(SQL);
				ps.setString(1,curso.getNmCurso());
				ps.setString(2,curso.getSiglaCursoSuap());
				ps.setInt(3,curso.getIdCurso());

				int affectedRows =ps.executeUpdate();

				if (affectedRows == 0) 
					throw new Exception("Não foi possivel excluir!");

			}catch (SQLException sqle){
				throw new Exception("Erro ao remover curso: \n"+sqle);
			}finally{
				ConnectionDAO.closeConnection(conn,ps);
			}

		}else {
			throw new Exception("Objeto Passado é nulo!");
		}
	}
	public void excluir(Object ob) throws Exception {
		Curso curso = (Curso) ob;

		if(curso != null) {
			PreparedStatement ps = null;
			Connection conn = null;
			ResultSet rs = null;
			try{
				conn = ConnectionDAO.getConnection();
				String SQL = "DELETE FROM `siscoord`.`Curso` WHERE `idCurso`= ?;";
				ps = conn.prepareStatement(SQL);
				ps.setInt(1,curso.getIdCurso());

				int affectedRows = ps.executeUpdate();

				if (affectedRows == 0) 
					throw new Exception("Não foi possivel excluir!");

			}catch (SQLException sqle){
				throw new Exception("Erro ao remover curso: \n"+sqle);
			}finally{
				ConnectionDAO.closeConnection(conn,ps);
			}

		}else {
			throw new Exception("Objeto Passado é nulo!");
		}
	}

	@Override
	public List listaTodos() throws Exception {
		PreparedStatement ps = null;
		Connection conn = null;
		ResultSet rs = null;
		ArrayList<Curso> cursos = new ArrayList<>();

		try{
			String SQL = "SELECT * FROM `Curso` INNER join Professor_has_Portaria "
					+ "on Curso.coordenador = Professor_has_Portaria.idProfessorHasPortaria "
					+ "INNER join Professor "
					+ "on Professor.idProfessor = Professor_has_Portaria.Professor_idProfessor "
					+ "INNER join Pessoa" + 
					"  on Pessoa.idPessoa = Professor.idPessoa";
			conn = this.conn;
			ps = conn.prepareStatement(SQL);
			rs = ps.executeQuery();
			while(rs.next()) {
				cursos.add(new Curso(rs.getInt(1),rs.getString(2),rs.getString(3), rs.getInt(8)));
			}
		}catch (SQLException sqle){
			throw new Exception("Erro ao carregar dados! \n"+sqle);
		}finally{
			ConnectionDAO.closeConnection(conn,ps);
		}
		return cursos;
	}

	@Override
	public List procura(Object ob) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void salvar(Object ob) throws Exception {
		Curso curso = (Curso) ob;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		try{
			conn = ConnectionDAO.getConnection();
			String SQL = "Insert into siscoord.Curso values (NULL, ? , ? , ?)";
			ps = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, curso.getNmCurso());
			ps.setString(2, curso.getSiglaCursoSuap());
			ps.setInt(3, curso.getCoordenador());
			//tratando da inserção e coletando último id;

			int affectedRows = ps.executeUpdate();

			if (affectedRows == 0) 
				throw new SQLException();

			rs = ps.getGeneratedKeys();
			if (rs.next()) {
				this.lastID = rs.getInt(1);
			}else{
				this.lastID = 0;
			}


		}catch (SQLException sqle){
			throw new Exception("Erro ao salvar dados do curso: \n"+sqle);
		}finally{
			ConnectionDAO.closeConnection(conn,ps);
		}
	}
	public int getLastID() {
		return this.lastID;
	}

}

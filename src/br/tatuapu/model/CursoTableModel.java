package br.tatuapu.model;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import br.tatuapu.model.Curso;

public class CursoTableModel extends AbstractTableModel {

	private ArrayList<Curso> dados;
	private String colunas[] = {"ID Curso", "Nome Curso", "Sigla", "Coordenador"};
	
	public CursoTableModel() {
		
		this.dados = new ArrayList<Curso>();
	}
	
	public void addRow(Curso c) {
		
		this.dados.add(c);
		this.fireTableDataChanged();
	}
	
	public void removeRow(int linha) {
		this.dados.remove(linha);
		this.fireTableRowsDeleted(linha, linha);
	}
	
	public int getColumnCount() {
		
		return this.colunas.length;
	}

	@Override
	public int getRowCount() {
		return this.dados.size();
	}
	
	public String getColumnName(int col) {
		return this.colunas[col];
	}

	@Override
	public Object getValueAt(int linha, int coluna) {
		
		switch(coluna) {
		
		case 0: return this.dados.get(linha).getIdCurso();
		case 1: return this.dados.get(linha).getNmCurso();
		case 2: return this.dados.get(linha).getSiglaCursoSuap();
		case 3: return this.dados.get(linha).getCoordenador();
		
		}
		return null;
	}
	public Curso getRowAt(int linha) {
		return this.dados.get(linha);
	}
	
      public ArrayList<Curso> getCurso() {
		
		return this.dados;
	}

}

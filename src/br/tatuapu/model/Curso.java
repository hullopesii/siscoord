package br.tatuapu.model;

import br.tatuapu.model.Coordenador;

public class Curso implements Coordenador{
	private int idCurso;
	private String nmCurso;
	private String siglaCursoSuap;
	private int coordenador;
	
	public Curso(int idCurso,String nmCurso, String sigla, int coordenador) {
		this.idCurso=idCurso;
		this.nmCurso=nmCurso;
		this.siglaCursoSuap=sigla;
		this.coordenador=coordenador;
		
	}

	public int getIdCurso() {
		return idCurso;
	}

	public String getNmCurso() {
		return nmCurso;
	}

	public String getSiglaCursoSuap() {
		return siglaCursoSuap;
	}

	public int getCoordenador() {
		return coordenador;
	}

	@Override
	public Portaria getPortaria() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setPortaria(Portaria p) {
		// TODO Auto-generated method stub
		
	}
	

}
package br.tatuapu.model;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import br.tatuapu.model.Portaria;

public class PortariaTableModel extends AbstractTableModel {

	private ArrayList<Portaria> dados;
	private String colunas[] = {"ID Portaria", "Numero Portaria", "SRC", "Tipo"};
	
	public PortariaTableModel() {
		
		this.dados = new ArrayList<Portaria>();
	}
	
	public void addRow(Portaria p) {
		
		this.dados.add(p);
		this.fireTableDataChanged();
	}
	
	public void removeRow(int linha) {
		this.dados.remove(linha);
		this.fireTableRowsDeleted(linha, linha);
	}
	
	public int getColumnCount() {
		
		return this.colunas.length;
	}

	@Override
	public int getRowCount() {
		return this.dados.size();
	}
	
	public String getColumnName(int col) {
		return this.colunas[col];
	}

	@Override
	public Object getValueAt(int linha, int coluna) {
		
		switch(coluna) {
		
		case 0: return this.dados.get(linha).getIdPortaria();
		case 1: return this.dados.get(linha).getNroPortaria();
		case 2: return this.dados.get(linha).getSrc();
		case 3: return this.dados.get(linha).getTipo();
		
		}
		return null;
	}
	
      public ArrayList<Portaria> getPortaria() {
		
		return this.dados;
	}
      
      public Portaria getRowAt(int linha) {
  		
  		return this.dados.get(linha);
  	}

}

package br.tatuapu.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import com.mysql.jdbc.Statement;

import br.tatuapu.util.ConnectionDAO;
import br.tatuapu.util.DAO;

//Wellinson Henrique de Souza

public class ExternoDAO implements DAO
{

	private Connection conn;
	private int lastID;
	
	public ExternoDAO() throws Exception {
		try {
			this.conn = ConnectionDAO.getConnection();
		} catch (Exception e) {
			throw new Exception("Erro: " + e.getMessage());
		}
	}

	public void atualizar(Object ob) throws Exception {
		Externo externo = (Externo) ob;

		if(externo != null) {
			PreparedStatement ps = null;
			Connection conn = null;
			try{

				conn = ConnectionDAO.getConnection();
				String SQL = "UPDATE siscoord.pessoa SET nmPessoa = ?, cpf = ? WHERE idPessoa = ?;";
				ps = conn.prepareStatement(SQL);
				ps.setString(1, externo.getNmPessoa());
				ps.setString(2, externo.getCpf());
				ps.setInt(3, externo.getIdPessoa());
				ps.execute();

				String SQLExterno = "UPDATE  siscoord.externo SET endereco = ?, telefone = ? WHERE idExterno = ?;";
				ps = conn.prepareStatement(SQLExterno);
				ps.setString(1, externo.getEndereco());
				ps.setString(2, externo.getTelefone());
				ps.setInt(3, externo.getIdExterno());
				ps.execute();

			}catch (SQLException sqle){
				throw new Exception("Erro ao atualizar: \n"+sqle);
			}finally{
				ConnectionDAO.closeConnection(conn,ps);
			}

		}else {
			throw new Exception("Objeto nulo!");
		}
	}

	@Override
	public void excluir(Object ob) throws Exception {
		Externo externo = (Externo) ob;
		if (externo != null) {
			PreparedStatement ps = null;
			ResultSet rs = null;
			Connection conn = null;

			try{

				String SQL = "DELETE FROM siscoord.externo WHERE idExterno = ?;";
				ps = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
				ps.setInt(1, externo.getIdExterno());
				
				int affectedRows = ps.executeUpdate();
				
				if(affectedRows == 0)
				{
					throw new Exception("Nao foi possivel excluir!");
				}


			}catch (SQLException sqle){
				throw new Exception("Erro ao excluwetfgair!! \n"+sqle);
			}finally{
				ConnectionDAO.closeConnection(conn,ps);
			}
		}
		else
			System.out.println("Objeto nulo!");
	}

	@Override
	public List<Externo> listaTodos() throws Exception {
		PreparedStatement ps = null;
		Connection conn = null;
		ResultSet rs = null;
		ArrayList<Externo> externo = new ArrayList<>();

		try {
			String SQL = "SELECT externo.idPessoa,pessoa.nmPessoa,pessoa.cpf,externo.idExterno,externo.endereco,externo.telefone FROM siscoord.externo INNER JOIN siscoord.pessoa ON siscoord.externo.idPessoa = siscoord.pessoa.idPessoa";
			conn = this.conn;
			ps = conn.prepareStatement(SQL);
			rs = ps.executeQuery();
			while (rs.next()) {
				Externo pEx = new Externo(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4),  rs.getString(5), rs.getString(6));
				externo.add(pEx);
			}
		} catch (SQLException sqle) {
			throw new Exception("Erro ao inserir dados do autor: \n" + sqle);
		} finally {
			ConnectionDAO.closeConnection(conn, ps);
		}
		return externo;
	}

	@Override
	public List<Externo> procura(Object ob) throws Exception {
		return null;
	}

	@Override
	public void salvar(Object ob) throws Exception 
	{
		Externo d = (Externo) ob;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			conn = ConnectionDAO.getConnection();
			
			PessoaDAO pessoaDao = new PessoaDAO();
			String[] pessoa = new String[3];
			pessoa[0]= "0";
			pessoa[1]= d.getNmPessoa();
			pessoa[2]= d.getCpf();
			
			List<Pessoa> pessoas = pessoaDao.procura(pessoa);
			int idPessoa;
			
			if(pessoas.size()==0) {
			
				String[] pessoaS = new String[2];
				pessoaS[0]= d.getNmPessoa();
				pessoaS[1]= d.getCpf();
				
				pessoaDao.salvar(pessoaS);
				idPessoa = pessoaDao.getLastID();
			}else {
				idPessoa = pessoas.get(0).getIdPessoa();
			}	
				
				String SQL = "INSERT INTO `siscoord`.`externo` VALUES (null,?,?,?);";
				ps = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);

				ps.setInt(1,idPessoa);
				ps.setString(2,d.getEndereco());
				ps.setString(3,d.getTelefone());

				int affectedRows = ps.executeUpdate();
					if (affectedRows == 0) {
					throw new SQLException();
				}
				rs = ps.getGeneratedKeys();
				if (rs.next()) {
					this.lastID = rs.getInt(1);
				} else {
					this.lastID = 0;
				}
				
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao Salvar Dados do Externo  : \n" + e);
		} finally {
			ConnectionDAO.closeConnection(conn, ps);
		}
	}
	public int getLastID(){
		return this.lastID;
	}

}
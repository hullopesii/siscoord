package br.tatuapu.model;

public class Setor {
	
	private int idSetor;
	private String nmSetor;
	private String nmSetorSuap;
	
	public Setor(int idSetor, String nmSetor, String nmSetorSuap) {
		this.idSetor = idSetor;
		this.nmSetor = nmSetor;
		this.nmSetorSuap = nmSetorSuap;
	}

	public int getIdSetor() {
		return idSetor;
	}
	
	public String getNmSetor() {
		return nmSetor;
	}
	
	public String getNmSetorSuap() {
		return nmSetorSuap;
	}
	
}
 	
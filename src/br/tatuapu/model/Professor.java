package br.tatuapu.model;

import java.util.ArrayList;
/*
 * @author Rúben França 
 */

public class Professor extends Pessoa implements Coordenador, Logavel {
	private final String area;
	private final String matricula;
	private final String user;
	private final String passWd;
	private final int idProfessor;
	private ArrayList<Curso> cursos;
	private Portaria portaria;

	/**
	 * 
	 * @param id - Id vindo da classe Pessoa
	 * @param nm - Nome que vem da classe Pessoa
	 * @param cpf - CPF que vem da classe Pessoa
	 * @param area
	 * @param matricula
	 * @param user
	 * @param passWd
	 * @param idProfessor
	 */
	public Professor(int id, String nm, String cpf, String area, String matricula, String user, String passWd,
			int idProfessor) {
		super(id, nm, cpf);
		this.area = area;
		this.matricula = matricula;
		this.user = user;
		this.passWd = passWd;
		this.idProfessor = idProfessor;
		cursos = new ArrayList<Curso>();
	}

	public String getArea() {
		return area;
	}

	public ArrayList<Curso> getCursos() {
		return cursos;
	}

	public void addCurso(Curso curso) {
		this.cursos.add(curso);
	}

	public String getMatricula() {
		return this.matricula;
	}

	@Override
	public Portaria getPortaria() {
		return this.getPortaria();
	}

	public String getUser() {
		return user;
	}

	public String getPassWd() {
		return passWd;
	}

	@Override
	public void setPortaria(Portaria p) {
		this.portaria = p;

	}

	public int getIdProfessor() {
		return this.idProfessor;
	}

	@Override
	public boolean login() {
		// TODO Auto-generated method stub
		return false;
	}

}

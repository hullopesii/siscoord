//Kaíque Matheus
package br.tatuapu.model;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import com.mysql.jdbc.Statement;

import br.tatuapu.util.ConnectionDAO;
import br.tatuapu.util.DAO;

public class DiscenteDAO implements DAO {

	private Connection conn;
	private int lastID;

	public void DiscenteDAO() throws Exception {
		try {
			this.conn = ConnectionDAO.getConnection();
		} catch (Exception e) {
			throw new Exception("Erro : \n" + e.getMessage());
		}
	}

	@Override
	public void atualizar(Object ob) throws Exception {
		Discente discente = (Discente) ob;

		if (discente != null) {

			PreparedStatement ps = null;
			Connection conn = null;
			ResultSet rs = null;

			try {
				conn = ConnectionDAO.getConnection();
				String SQL = "UPDATE `siscoord`.`Pessoa` SET `nmPessoa` = ? , `cpf` = ? WHERE `idPessoa` = ?;";
				// String SQL = "UPDATE `siscoord`.`Pessoa` SET `nmPessoa` = ?, `cpf` = ? WHERE
				// `idPessoa` = (SELECT `Discente`.`idPessoa` FROM `siscoord`.`Discente` WHERE
				// `Discente`.`idDiscente` = ?);";

				ps = conn.prepareStatement(SQL);
				ps.setString(1, discente.getNmPessoa());
				ps.setString(2, discente.getCpf());
				ps.setInt(3, discente.getIdPessoa());
				ps.execute();
				// int affectedRows = ps.executeUpdate();

				/*
				 * if (affectedRows == 0) throw new Exception("O Discente não foi alterado!");
				 */} catch (SQLException sqle) {
				throw new Exception("Erro ao salvar dados do Discente: \n" + sqle);
			} finally {
				ConnectionDAO.closeConnection(conn, ps);
			}
		} else {
			throw new Exception("Objeto passado é nulo!");
		}
	}

	@Override
	public void excluir(Object ob) throws Exception {
		Discente discente = (Discente) ob;

		if (discente != null) {
			PreparedStatement ps = null;
			Connection conn = null;
			ResultSet rs = null;

			try {
				conn = ConnectionDAO.getConnection();
				String SQL1 = "DELETE FROM `siscoord`.`Discente` WHERE idDiscente = ? ;";
				ps = conn.prepareStatement(SQL1, Statement.RETURN_GENERATED_KEYS);
				ps.setInt(1, discente.getIdDiscente());

				int affectedRows = ps.executeUpdate();
				if (affectedRows == 0) {
					throw new Exception("Não foi Possivel excluir!");
				}

			} catch (SQLException sqle) {
				throw new Exception("Erro ao excluir registro! \n Registro passado é nulo!");

			} finally {
				ConnectionDAO.closeConnection(conn, ps);
			}

		} else {
			System.out.println("Objeto NULO");
		}

	}

	@Override
	public List<Discente> listaTodos() throws Exception {
		PreparedStatement ps = null;
		Connection conn = null;
		ResultSet rs = null;
		ArrayList<Discente> discentes = new ArrayList<Discente>();
		try {
			String SQL = "Select `Discente`.`idDiscente`,`Discente`.`idPessoa`,`Pessoa`.`nmPessoa`,`Pessoa`.`cpf` from Discente INNER JOIN Pessoa ON Pessoa.idPessoa=Discente.idPessoa;";
			conn = ConnectionDAO.getConnection();
			ps = conn.prepareStatement(SQL);
			rs = ps.executeQuery();
			while (rs.next()) {
				discentes.add(new Discente(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4)));
			}
		} catch (SQLException sqle) {
			throw new Exception("Erro ao buscar dados do Discente: \n" + sqle);
		} finally {
			ConnectionDAO.closeConnection(conn, ps);
		}
		return discentes;
	}

	@Override
	public List<Discente> procura(Object ob) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void salvar(Object ob) throws Exception {
		Discente d = (Discente) ob;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			conn = ConnectionDAO.getConnection();

			PessoaDAO pessoaDao = new PessoaDAO();
			String[] pessoa = new String[3];
			pessoa[0] = "0";
			pessoa[1] = d.getNmPessoa();
			pessoa[2] = d.getCpf();

			List<Discente> pessoas = pessoaDao.procura(pessoa);
			int idPessoa;

			if (pessoas.size() == 0) {

				String[] pessoaS = new String[2];
				pessoaS[0] = d.getNmPessoa();
				pessoaS[1] = d.getCpf();

				pessoaDao.salvar(pessoaS);
				idPessoa = pessoaDao.getLastID();
			} else {
				idPessoa = pessoas.get(0).getIdPessoa();
			}

			String SQL = "INSERT INTO `siscoord`.`Discente` (`idPessoa`) VALUES (?);";
			ps = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, idPessoa);
			int affectedRows = ps.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException();
			}
			rs = ps.getGeneratedKeys();
			if (rs.next()) {
				this.lastID = rs.getInt(1);
			} else {
				this.lastID = 0;
			}

		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao Salvar Dados do Discente: \n" + e);
		} finally {
			ConnectionDAO.closeConnection(conn, ps);
		}
	}

	public int getLastID() {
		return this.lastID;
	}

}
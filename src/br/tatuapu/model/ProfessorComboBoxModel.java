package br.tatuapu.model;

import java.util.ArrayList;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

public class ProfessorComboBoxModel extends AbstractListModel implements ComboBoxModel{

	private ArrayList<Professor> dados;
	private Professor selection;
	
	public ProfessorComboBoxModel() {
		
		this.dados = new ArrayList<Professor>();
	}
	
	public void addItem(Professor p) {
		
		this.dados.add(p);
	}
	
	@Override
	public int getSize() {
		return this.dados.size();				
	}
	
    public ArrayList<Professor> getCurso() {
    	
		return this.dados;
	}

	@Override
	public Object getElementAt(int index) {
		return this.dados.get(index);
	}
	

	public Professor getSelectedItem() {
		return selection;
	}

	public void setSelectedItem(Object p) {
		selection = (Professor) p;		
	}

	
	
	
}

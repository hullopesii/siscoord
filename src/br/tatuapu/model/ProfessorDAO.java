package br.tatuapu.model;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTable;

import com.mysql.jdbc.Statement;

import br.tatuapu.util.ConnectionDAO;
import br.tatuapu.util.DAO;

public class ProfessorDAO implements DAO {
	private Connection conn;
	private int lastID;

	public ProfessorDAO () throws Exception {
		try{
			this.conn = ConnectionDAO.getConnection();
		}catch(Exception e){
			throw new Exception("Erro: \n"+e.getMessage());                    
		}
	}

	@Override
	public void atualizar(Object ob) throws Exception {
		Professor prof = (Professor) ob;

		if(prof != null) {
			PreparedStatement ps = null;
			Connection conn = null;
			try{

				conn = ConnectionDAO.getConnection();
				String SQL = "UPDATE siscoord.Pessoa SET nmPessoa = ?, cpf = ? WHERE idPessoa = ?;";
				ps = conn.prepareStatement(SQL);
				ps.setString(1, prof.getNmPessoa());
				ps.setString(2, prof.getCpf());
				ps.setInt(3, prof.getIdPessoa());
				ps.execute();

				String SQLProfessor = "UPDATE  siscoord.Professor SET area = ?, matricula = ? WHERE idProfessor = ?;";
				ps = conn.prepareStatement(SQLProfessor);
				ps.setString(1, prof.getArea());
				ps.setString(2, prof.getMatricula());
				ps.setInt(3, prof.getIdProfessor());
				ps.execute();

			}catch (SQLException sqle){
				throw new Exception("Erro ao atualizar curso: \n"+sqle);
			}finally{
				ConnectionDAO.closeConnection(conn,ps);
			}

		}else {
			throw new Exception("Objeto Passado é nulo!");
		}
	}

	@Override
	public void excluir(Object ob) throws Exception {
		Professor prof = (Professor) ob;
		if (prof != null) {
			PreparedStatement ps = null;
			ResultSet rs = null;
			Connection conn = null;

			try{
				String SQL1 = "DELETE FROM siscoord.Professor_has_Portaria WHERE Professor_idProfessor = ?;";
				conn = ConnectionDAO.getConnection();
				ps = conn.prepareStatement(SQL1);
				ps.setInt(1, prof.getIdProfessor());
				ps.execute();
				System.out.println(SQL1+prof.getIdProfessor());

				String SQL2 = "DELETE FROM siscoord.Professor WHERE idProfessor = ?;";
				ps = conn.prepareStatement(SQL2);
				ps.setInt(1, prof.getIdProfessor());
				ps.execute();
				System.out.println(SQL2 + prof.getIdProfessor());

			}catch (SQLException sqle){
				throw new Exception("Erro ao excluir Professor \n"+sqle);
			}finally{
				ConnectionDAO.closeConnection(conn,ps);
			}
		}
		else
			System.out.println("Objeto NULO!");
	}

	@Override
	public List<Professor> listaTodos() throws Exception {
		PreparedStatement ps = null;
		java.sql.Connection conn = null;
		ResultSet rs;
		ArrayList<Professor> professores = new ArrayList<Professor>();
		try{
			String SQL = "SELECT * FROM Professor INNER JOIN Pessoa on Pessoa.idPessoa = Professor.idPessoa";
			conn = this.conn;
			ps = conn.prepareStatement(SQL);
			rs = ps.executeQuery();
			while(rs.next()) {
				professores.add(new Professor(rs.getInt(2), rs.getString(8), rs.getString(9), rs.getString(4), rs.getString(3),rs.getString(5), rs.getString(6),rs.getInt(1)));
			}
		}catch (SQLException sqle){
			throw new Exception("Erro ao inserir dados do autor: \n"+sqle);
		}finally{
			ConnectionDAO.closeConnection(conn,ps);
		}

		return professores;
	}

	@Override
	public List<Professor> procura(Object ob) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void salvar(Object ob) throws Exception {
		Professor prof = (Professor) ob;

		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		PessoaDAO pessoaBD = new PessoaDAO();
		try{
			if (pessoaBD.procura(prof).size() == 0 ) {

				conn = ConnectionDAO.getConnection();
				String SQL = "INSERT INTO Professor VALUES (null,?,?,?,?,'123456');";
				ps = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
				ps.setInt(1, prof.getIdPessoa());
				ps.setString (2, prof.getMatricula());
				ps.setString(3, prof.getArea());
				ps.setString(4, prof.getCpf());

				//tratando da inserção e coletando último id;

				int affectedRows = ps.executeUpdate();

				if (affectedRows == 0) 
					throw new SQLException();

				rs = ps.getGeneratedKeys();
				if (rs.next()) {
					this.lastID = rs.getInt(1);
				}else{
					this.lastID = 0;
				}

			}else {
				Discente d = (Discente) pessoaBD.procura(prof).get(0);
				
				conn = ConnectionDAO.getConnection();
				String SQL = "INSERT INTO Professor VALUES (null,?,?,?,?,'123456');";
				ps = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
				ps.setInt(1,d.getIdPessoa());
				ps.setString (2, prof.getMatricula());
				ps.setString(3, prof.getArea());
				ps.setString(4, d.getCpf());

				//tratando da inserção e coletando último id;

				int affectedRows = ps.executeUpdate();

				if (affectedRows == 0) 
					throw new SQLException();

				rs = ps.getGeneratedKeys();
				if (rs.next()) {
					this.lastID = rs.getInt(1);
				}else{
					this.lastID = 0;
				}
			}
		}catch (SQLException sqle){
			throw new Exception("Erro ao salvar dados dos tipos de documentos: \n"+sqle);
		}finally{
			ConnectionDAO.closeConnection(conn,ps);
		}
	}

	public int getLastID(){
		return this.lastID;
	}


}

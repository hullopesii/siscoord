package br.tatuapu.model;

import br.tatuapu.*;
import br.tatuapu.util.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tatuapu
 */
public class AutorDAO  implements DAO{
    private Connection conn;
    
    public AutorDAO() throws Exception{
        try{
            this.conn = ConnectionDAO.getConnection();
        }catch(Exception e){
            throw new Exception("Erro: \n"+e.getMessage());                    
        }
    }
    @Override
    public void salvar(Object ob) throws Exception{
        PreparedStatement ps = null;
        Connection conn = null;
       
        try{
            String SQL = "INSERT INTO autor (nomeAutor, nmCitacao) VALUES (?,?)";
            conn = this.conn;
            ps = conn.prepareStatement(SQL);
            ps.setString(1, "");
            ps.setString(2, "");
            ps.executeUpdate();
        }catch (SQLException sqle){
            throw new Exception("Erro ao inserir dados do autor: \n"+sqle);
        }finally{
            ConnectionDAO.closeConnection(conn,ps);
        }
    }
    
    @Override
    public void excluir(Object ob) throws Exception{
       //
    }
    @Override
    public void atualizar(Object ob) throws Exception{
        //
    }
    @Override
    public List listaTodos() throws Exception{
        //
    	return new ArrayList();
    }
    @Override
    public List procura(Object ob) throws Exception{
        //
    	return new ArrayList();
    }
}

package br.tatuapu.model;

public class Portaria {

	private final int idPortaria;
	private final String nroPortaria;
	private  String src;
	private final String tipo;
	
	public Portaria(int idPortaria, String nroPortaria, String tipo, String src) {
		
		this.idPortaria = idPortaria;
		this.nroPortaria = nroPortaria;
		this.tipo = tipo;
		this.src = src;
		
	}
	
	public int getIdPortaria() {
		
		return this.idPortaria;
	}
	
	public String getNroPortaria() {
		
		return this.nroPortaria;
	}
	
	public String getSrc() {
		
		return this.src;
	}
	
	public void setSrc(String src) {
		
		this.src = src;
	}
	
	public String getTipo() {
		
		return this.tipo;
	}
	
	
}

package br.tatuapu.model;

import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Statement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.tatuapu.util.ConnectionDAO;
import br.tatuapu.util.DAO;

public class SetorDAO implements DAO {

	private Connection conn;
	private int lastID;
	
	@Override
	public void atualizar(Object ob) throws Exception {
		
		Setor setor = (Setor) ob;
		
		if(setor != null){
			
			PreparedStatement ps = null;
	        Connection conn = null;
	        ResultSet rs = null;

	        try{
	         	conn = ConnectionDAO.getConnection();
	         	
	        	String SQL = "UPDATE siscoord.Setor set nmSetor = ?, siglaSetorSuap = ? WHERE idSetor = ?";
	        	
	        	ps = conn.prepareStatement(SQL);
	        	
	        	ps.setString(1,setor.getNmSetor());
	        	ps.setString(2, setor.getNmSetorSuap());
	        	ps.setInt(3, setor.getIdSetor());
	              	
	        	int affectedRows = ps.executeUpdate();
	    	
		    	if (affectedRows == 0) 
		            throw new Exception("O Setor não foi alterado!");
	    	
		    }
	        
	        catch (SQLException sqle) {
		        
	        	throw new Exception("Erro ao salvar dados do Setor: \n"+sqle);
	        	
		    }
	        
	        finally{
		        
	        	ConnectionDAO.closeConnection(conn,ps);
	        	
		    }
	        
		}else{
			
			throw new Exception("Objeto passado é nulo!");
			
		}
	}

	@Override
	public void excluir(Object ob) throws Exception {
		Setor setor = (Setor) ob;
		
		if(setor != null){
			PreparedStatement ps = null;
	        Connection conn = null;
	        ResultSet rs = null;

	        try{
	         	conn = ConnectionDAO.getConnection();
	        	String SQL = "DELETE FROM `siscoord`.`Setor` WHERE `idSetor`= ?;";
	        	ps = conn.prepareStatement(SQL);
	        	ps.setInt(1,setor.getIdSetor());
	              	
	        	int affectedRows = ps.executeUpdate();
	    	
		    	if (affectedRows == 0) 
		            throw new Exception("O Setor não foi removido!");
	    	
		    }catch (SQLException sqle){
		        throw new Exception("Erro ao salvar dados do Setor: \n"+sqle);
		    }finally{
		        ConnectionDAO.closeConnection(conn,ps);
		    }
		}else{
			throw new Exception("Objeto passado é nulo!");
		}
	}
	
	
	public SetorDAO() throws Exception {
		try {
			this.conn = ConnectionDAO.getConnection();
        }catch(Exception e){
            throw new Exception("Erro: \n"+e.getMessage());                    
        }
	}


	@Override
	public List listaTodos() throws Exception {
		PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        ArrayList<Setor> setores = new ArrayList<>();
        try{
            String SQL = "Select * from Setor";
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement(SQL);
            rs = ps.executeQuery();
            while(rs.next()) {
            	Setor set = new Setor(rs.getInt(1),rs.getString(2),rs.getString(3));
            	setores.add(set);              	
//            	if(rs.getString(5).equals("Professor")){
//            		SQL = "Select * from Setor inner join Professor on Professor.idProfessor = "
//            				+ "Setor.idCoordenador";
//            		ps = conn.prepareStatement(SQL);
//            		ResultSet rs2 = ps.executeQuery();
//            		while(rs.next()){
//            			Setor set = new Setor(rs.getInt(1),rs.getString(2),rs.getString(3));
//                    	setores.add(set);
//            		}
//            	}else{
//            		
//            	}
            }
        }catch (SQLException sqle){
            throw new Exception("Erro ao buscar dados do Setor: \n"+sqle);
        }finally{
            ConnectionDAO.closeConnection(conn,ps);
        }
		return setores;
	}

	@Override
	public List procura(Object ob) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void salvar(Object ob) throws Exception {
		Setor setor = (Setor) ob;
		PreparedStatement ps = null;
		ResultSet rs = null;
        Connection conn = null;
        try{
        	conn = ConnectionDAO.getConnection();
        	String SQL = "Insert into siscoord.Setor(idSetor,nmSetor,siglaSetorSuap) values (NULL, ?,?)";
        	ps = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);

        	ps.setString(1, setor.getNmSetor());
        	ps.setString(2, setor.getNmSetorSuap());
        	
        	
        	//tratando da inserção e coletando último id;
        	
        	int affectedRows = ps.executeUpdate();
        	
        	if (affectedRows == 0) 
                throw new SQLException();
        	
        	rs = ps.getGeneratedKeys();
        	if (rs.next()) {
                this.lastID = rs.getInt(1);
            }else{
            	this.lastID = 0;
            }
            
        	
        }catch (SQLException sqle){
            throw new Exception("Erro ao salvar dados do setor: \n"+sqle);
        }finally{
            ConnectionDAO.closeConnection(conn,ps);
        }
	}
	
	public int getLastID(){
		return this.lastID;
	}

}

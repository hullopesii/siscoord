package br.tatuapu.util;

import javax.swing.JTable;

public interface JanelaPadraoComTabela {
	public void btnLimparActionPerformed();
	public void btnDeletarActionPerformed();
	public void btnMaisActionPerformed();
	public void limpaTela();
	public JTable getTabela();
	public void preencheCampos();
	public void preencheTabela();
}

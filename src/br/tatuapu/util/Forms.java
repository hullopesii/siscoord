package br.tatuapu.util;

import java.awt.*;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 *
 * @author tatuapu
 */
public class Forms {
    
    /**
     * to clear all textFields(JTextField) of an container
     * to use, use getContentPane(). Ex.: Forms.clearTextFields(getContentPane());
     * @param container 
     */
    public static void clearTextFields(Container container) {
        for (Component c : container.getComponents()) {
            if (c instanceof JTextField) {
                JTextField f = (JTextField) c;
                f.setText("");
            } else if (c instanceof Container) {
                clearTextFields((Container) c);
            }
        }
    }

    /**
     * retorna uma lista com os campos obrigatórios que estão em branco
     * @param painelSuperior container alvo
     * @param camposObrigatorios ArrayList com os componentes que são obrigatórios
     * @return lista de componentes obrigatórios em branco
     */
	public static ArrayList checaCamposVazios(Container container, ArrayList camposObrigatorios) {
		ArrayList campos = new ArrayList();
		
		for (Component c : container.getComponents()) {
            if (c instanceof JTextField) {
                JTextField f = (JTextField) c;
                if(camposObrigatorios.contains(f) && f.getText().equals(""))
                	campos.add(f);
            } else if (c instanceof Container) {
                clearTextFields((Container) c);
            }
        }
		
		return campos;
	}
	
	/**
	 * Ajusta automaticamente a largura das colunas
	 * @param tabela - entra com a tabela que deseja ajustar. Recomenda-se que seja 
	 * passada após a inserção de dados na model, pois é com isto que se define sua largura ideal.
	 */
	public static void ajustaColunasTabela(JTable tabela){
		tabela.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		TableColumnAdjuster tca = new TableColumnAdjuster( tabela, 0 );
		tca.adjustColumns();
	}
}
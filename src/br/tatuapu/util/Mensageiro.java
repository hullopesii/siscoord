package br.tatuapu.util;

import javax.swing.*;

public class Mensageiro {
	
	/**
	 * 
	 * @param tipo indica um tipo de ação, sendo 
	 * 		<b>novo</b> para novo registro
	 * 		<b>alteração</b> para update de registro
	 * 		<b>remoção</b> para delete de registro
	 * @param nmModulo indicando o nome do módulo solicitante
	 */
	public static void mostraMensagemSucesso(String tipo, String nmModulo) {
		switch(tipo){
			case "novo":
				JOptionPane.showMessageDialog(null,"Dados cadastrados com sucesso!", nmModulo, JOptionPane.INFORMATION_MESSAGE);
				break;
			case "alteracao":
				JOptionPane.showMessageDialog(null,"Dados alterados com sucesso!", nmModulo, JOptionPane.INFORMATION_MESSAGE);
				break;
			case "remocao":
				JOptionPane.showMessageDialog(null,"Dados excluidos com sucesso!", nmModulo, JOptionPane.INFORMATION_MESSAGE);
				break;
		}
				
	}
	/**
	 * 
	 * @param tipo indica um tipo de ação, sendo 
	 * 		<b>novo</b> para novo registro
	 * 		<b>alteração</b> para update de registro
	 * 		<b>remoção</b> para delete de registro
	 * 		<b>listar</b> para listagem de registros
	 * @param nmModulo indicando o nome do módulo solicitante
	 * @param msgError indicando mensagem de erro
	 */
	public static void mostraMensagemErro(String tipo, String nmModulo, String msgError) {
		switch(tipo){
			case "novo":
				JOptionPane.showMessageDialog(null,"Erro ao salvar novo registro: \n"+msgError, nmModulo, JOptionPane.ERROR_MESSAGE);
				break;
			case "alteração":
				JOptionPane.showMessageDialog(null,"Erro ao salvar dados da alteração: \n"+msgError, nmModulo, JOptionPane.INFORMATION_MESSAGE);
				break;
			case "remoção":
				JOptionPane.showMessageDialog(null,"Erro ao excluir dados: \n"+msgError, nmModulo, JOptionPane.INFORMATION_MESSAGE);
				break;
			case "listar":
				JOptionPane.showMessageDialog(null,"Erro ao listar dados: \n"+msgError, nmModulo, JOptionPane.INFORMATION_MESSAGE);
				break;	
		}
				
	}
	
	/**
	 * Mostra mensagem genérica para formulário com campos vazios!
	 * @param nmModulo carregando o nome do módulo
	 * @param ob sem definição ainda
	 */
	public static void mostraMensagemFormularioVazio(String nmModulo, Object ob) {
		JOptionPane.showMessageDialog(null,"Preencha todos os dados do formulário!", nmModulo, JOptionPane.INFORMATION_MESSAGE);		
	}
	public static void mostraMensagemGenerica(String msg) {
		JOptionPane.showMessageDialog(null,msg);		
	}
	/**
	 * Mensagem de confirmação básica
	 * @param msg com a mensagem a ser apresentada
	 * @return int, contendo a opção selecionada pelo usuário
	 */
	public static int Confirm(String msg) {
		int opcao = JOptionPane.showConfirmDialog(null, msg);
       
		return opcao;
	}

}
